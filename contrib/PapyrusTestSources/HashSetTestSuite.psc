scriptName HashSetTestSuite extends AutoTestSuite hidden

function TestDefaultConstructor()
    HashSet set = HashSet.Create()
    Assert.IsObjectNotNone(set)
    Assert.AreIntsEqual(0, set.Size, "Set should have a size of 0.")
    Assert.IsTrue(set.Empty)
endFunction

function TestReservingConstructor()
    HashSet set = HashSet.Create(16)
    Assert.IsObjectNotNone(set)
    Assert.AreIntsEqual(0, set.Size, "Set should have a size of 0.")
    Assert.IsTrue(set.Empty)
endFunction

function TestReserve()
    HashSet set = HashSet.Create(16)
    Assert.IsObjectNotNone(set)
    set.Reserve(16)
endFunction

function TestGetNonExistent()
    HashSet set = HashSet.Create()
    Assert.IsObjectNone(set.Get(Any.OfInt(10)))
endFunction

function TestPutGet()
    HashSet set = HashSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Set should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Assert.AreIntsEqual(10, set.Get(Any.OfInt(10)).GetInt())
endFunction

function TestPutContains()
    HashSet set = HashSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Set should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Assert.IsTrue(set.Contains(Any.OfInt(10)))
endFunction

function TestPutOverwrite()
    HashSet set = HashSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Any overwritten = set.Put(Any.OfInt(10))
    Assert.AreIntsEqual(10, overwritten.GetInt())
    Assert.AreIntsEqual(10, set.Get(Any.OfInt(10)).GetInt())
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
endFunction

function TestTryPutGet()
    HashSet set = HashSet.Create()
    Assert.IsTrue(set.TryPut(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Assert.AreIntsEqual(10, set.Get(Any.OfInt(10)).GetInt())
endFunction

function TestTryPutOverwrite()
    HashSet set = HashSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
    Assert.IsFalse(set.TryPut(Any.OfInt(10)))
    Assert.AreIntsEqual(10, set.Get(Any.OfInt(10)).GetInt())
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.IsFalse(set.Empty)
endFunction

function TestDelete()
    HashSet set = HashSet.Create()
    Assert.IsObjectNone(set.Put(Any.OfInt(10)))
    Assert.AreIntsEqual(1, set.Size, "Map should have a size of 1.")
    Assert.AreIntsEqual(10, set.Delete(Any.OfInt(10)).GetInt())
    Assert.AreIntsEqual(0, set.Size, "Map should be empty.")
    Assert.IsTrue(set.Empty)
endFunction

