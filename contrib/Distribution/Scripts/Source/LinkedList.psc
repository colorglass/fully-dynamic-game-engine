scriptName LinkedList extends Collection hidden

LinkedList function Create() global native

function Append(Any value) native

function Prepend(Any value) native

Any function PopFront() native

Any function PopBack() native

LinkedListIterator function GetListIterator() native

LinkedListIterator function GetReverseListIterator() native

ValueIterator function GetReverseValueIterator() native

LinkedListIterator function GetListIteratorFrom(Int index) native

ValueIterator function GetValueIteratorFrom(Int index) native

LinkedListIterator function GetReverseListIteratorFrom(Int index) native

ValueIterator function GetReverseValueIteratorFrom(Int index) native
