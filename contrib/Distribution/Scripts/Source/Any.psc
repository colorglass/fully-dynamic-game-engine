scriptName Any extends ScriptObject hidden

Any function Empty() global native
Any function OfBool(Bool value = False) global native
Any function OfInt(Int value = 0) global native
Any function OfFloat(Float value = 0.0) global native
Any function OfString(String value = "") global native
Any function OfObject(ScriptObject value = None) global native
Any function OfForm(Form value = None) global native
Any function OfActiveMagicEffect(ActiveMagicEffect value = None) global native
Any function OfAlias(Alias value = None) global native
Any function OfBools(Bool[] values = None) global native
Any function OfInts(Int[] values = None) global native
Any function OfFloats(Float[] values = None) global native
Any function OfStrings(String[] values = None) global native
Any function OfObjects(ScriptObject[] values = None) global native
Any function OfForms(Form[] values = None) global native
Any function OfActiveMagicEffects(ActiveMagicEffect[] values = None) global native
Any function OfAliases(Alias[] value = None) global native
Any function Error(Error exception) global native

Bool property IsNone
    Bool function get()
        return __IsNone()
    endFunction
endProperty
Bool function __IsNone() native

Bool property IsBool
    Bool function get()
        return __IsBool()
    endFunction
endProperty
Bool function __IsBool() native

Bool property IsInt
    Bool function get()
        return __IsInt()
    endFunction
endProperty
Bool function __IsInt() native

Bool property IsFloat
    Bool function get()
        return __IsFloat()
    endFunction
endProperty
Bool function __IsFloat() native

Bool property IsString
    Bool function get()
        return __IsString()
    endFunction
endProperty
Bool function __IsString() native

Bool property IsObject
    Bool function get()
        return __IsObject()
    endFunction
endProperty
Bool function __IsObject() native

Bool property IsForm
    Bool function get()
        return __IsForm()
    endFunction
endProperty
Bool function __IsForm() native

Bool property IsActiveMagicEffect
    Bool function get()
        return __IsActiveMagicEffect()
    endFunction
endProperty
Bool function __IsActiveMagicEffect() native

Bool property IsAlias
    Bool function get()
        return __IsAlias()
    endFunction
endProperty
Bool function __IsAlias() native

Bool property IsBools
    Bool function get()
        return __IsBools()
    endFunction
endProperty
Bool function __IsBools() native

Bool property IsInts
    Bool function get()
        return __IsInts()
    endFunction
endProperty
Bool function __IsInts() native

Bool property IsFloats
    Bool function get()
        return __IsFloats()
    endFunction
endProperty
Bool function __IsFloats() native

Bool property IsStrings
    Bool function get()
        return __IsStrings()
    endFunction
endProperty
Bool function __IsStrings() native

Bool property IsObjects
    Bool function get()
        return __IsObjects()
    endFunction
endProperty
Bool function __IsObjects() native

Bool property IsForms
    Bool function get()
        return __IsForms()
    endFunction
endProperty
Bool function __IsForms() native

Bool property IsActiveMagicEffects
    Bool function get()
        return __IsActiveMagicEffects()
    endFunction
endProperty
Bool function __IsActiveMagicEffects() native

Bool property IsAliases
    Bool function get()
        return __IsAliases()
    endFunction
endProperty
Bool function __IsAliases() native

Bool property IsError
    Bool function get()
        return __IsError()
    endFunction
endProperty
Bool function __IsError() native

Bool function GetBool() native
Int function GetInt() native
Float function GetFloat() native
String function GetString() native
ScriptObject function GetObject() native
Form function GetForm() native
ActiveMagicEffect function GetActiveMagicEffect() native
Alias function GetAlias() native
Bool[] function GetBools() native
Int[] function GetInts() native
Float[] function GetFloats() native
String[] function GetStrings() native
ScriptObject[] function GetObjects() native
Form[] function GetForms() native
ActiveMagicEffect[] function GetActiveMagicEffects() native
Alias[] function GetAliases() native
ScriptObject function GetError() native

Bool property IsIntCompatible
    Bool function get()
        return __IsIntCompatible()
    endFunction
endProperty
Bool function __IsIntCompatible() native

Bool property IsFloatCompatible
    Bool function get()
        return __IsFloatCompatible()
    endFunction
endProperty
Bool function __IsFloatCompatible() native

Bool property AsBool
    Bool function get()
        return __AsBool()
    endFunction
endProperty
Bool function __AsBool() native

Int property AsInt
    Int function get()
        return __AsInt()
    endFunction
endProperty
Int function __AsInt() native

Float property AsFloat
    Float function get()
        return __AsFloat()
    endFunction
endProperty
Float function __AsFloat() native

String property AsString
    String function get()
        return __AsString()
    endFunction
endProperty
String function __AsString() native
