scriptName Regex extends ScriptObject hidden

Regex function Create(String pattern, Bool optimize = True) global native

String property Pattern
    String function get()
        return __GetPattern()
    endFunction
endProperty
String function __GetPattern() native

Bool property IsOptimized
    Bool function get()
        return __IsOptimized()
    endFunction
endProperty
Bool function __IsOptimized() native

Bool function IsExactMatch(String pattern) native

Bool function IsMatch(String pattern) native

MatchResult[] function Match(String pattern) native

MatchResult[] function ExactMatch(String pattern) native

String function Replace(String pattern, String replacement, Bool firstOnly = false, Bool noCopy = false) native
