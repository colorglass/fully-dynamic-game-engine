scriptName HashSet extends SetCollection hidden

HashSet function Create(Int initialCapacity = 0) global native

Int property Capacity
    Int function get()
        return __GetCapacity()
    endFunction
endProperty
Int function __GetCapacity() native

function Reserve(Int capacity) native

ValueIterator function GetReverseValueIterator() native

ValueIterator function GetReverseValueIteratorFrom(Any value) native
