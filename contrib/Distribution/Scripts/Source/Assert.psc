scriptName Assert hidden

Bool function IsTrue(Bool value, String errorMessage = "") global native

Bool function IsFalse(Bool value, String errorMessage = "") global native

Bool function AreIntsEqual(Int a, Int b, String errorMessage = "") global native

Bool function AreFloatsEqual(Float a, Float b, String errorMessage = "") global native

Bool function AreStringsEqual(String a, String b, String errorMessage = "") global native

Bool function AreObjectsEqual(ScriptObject a, ScriptObject b, String errorMessage = "") global native

Bool function AreFormsEqual(Form a, Form b, String errorMEssage = "") global native

Bool function AreActiveMagicEffectsEqual(ActiveMagicEffect a, ActiveMagicEffect b, String errorMessage = "") global native

Bool function AreAliasesEqual(Alias a, Alias b, String errorMessage = "") global native

Bool function IsObjectNone(ScriptObject value, String errorMessage = "") global native

Bool function IsFormNone(Form value, String errorMessage = "") global native

Bool function IsActiveMagicEffectNone(ActiveMagicEffect value, String errorMessage = "") global native

Bool function IsAliasNone(Alias value, String errorMessage = "") global native

Bool function IsObjectNotNone(ScriptObject value, String errorMessage = "") global native

Bool function IsFormNotNone(Form value, String errorMessage = "") global native

Bool function IsActiveMagicEffectNotNone(ActiveMagicEffect value, String errorMessage = "") global native

Bool function IsAliasNotNone(Alias value, String errorMessage = "") global native
