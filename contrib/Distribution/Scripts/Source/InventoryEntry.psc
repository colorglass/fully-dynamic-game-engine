scriptName InventoryEntry extends ScriptObject

InventoryEntry[] function GetInventoryFor(ObjectReference target) global native

Int property Count
    Int function get()
        return __GetCount()
    endFunction
endProperty
Int function __GetCount() native

ObjectReference property BoundReference
    ObjectReference function get()
        return __GetBoundReference()
    endFunction
endProperty
ObjectReference function __GetBoundReference() native

Form property BoundForm
    Form function get()
        return __GetBoundForm()
    endFunction
endProperty
Form function __GetBoundForm() native

Bool property Stolen
    Bool function get()
        return __IsStolen()
    endFunction
    function set(Bool value)
        __SetStolen(value)
    endFunction
endProperty
Bool function __IsStolen() native
Bool function __SetStolen(Bool value) native

function Remove(Int count = 1) native

function Add(Int count = 1) native

function TransferTo(ObjectReference other, Int count = 1) native
