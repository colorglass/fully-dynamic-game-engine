scriptName TestSuite extends ScriptObject hidden

Bool function RunAll(Bool allAsserts = false, String testGroupFilter = "", String testSuiteFilter = "", String testFilter = "") global native

Bool function Run(Bool allAsserts = false) native

event OnPreTestSuite()
endEvent

event OnPostTestSuite()
endEvent

event OnPreTest()
endEvent

event OnPostTest()
endEvent
