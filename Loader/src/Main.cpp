#include <errhandlingapi.h>
#include <FDGE/Config/DynamicFileProxy.h>
#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>

using namespace FDGE;
using namespace FDGE::Config;

namespace {
    constexpr std::string_view DLLPath = R"(Data\SKSE\Plugins\FullyDynamicGameEngine\FullyDynamicGameEngine.dll)";

    OnSKSELoading {
        auto* fudge = GetModuleHandleA("FullyDynamicGameEngine.dll");
        auto* load = reinterpret_cast<bool(*)(const SKSE::LoadInterface&)>(
            GetProcAddress(fudge, "SKSEPlugin_Load"));
        if (!load) {
            throw std::runtime_error("Failed to load Fully Dynamic Game Engine.");
        }
        if (!load(LoadInterface)) {
            throw PluginIncompatible();
        }
    }
}

BOOL WINAPI DllMain(HINSTANCE, DWORD fdwReason, LPVOID) {
    if (fdwReason != DLL_PROCESS_ATTACH) {
        return true;
    }
    if (!SetDllDirectoryA(R"(Data\SKSE\Plugins\FullyDynamicGameEngine)")) {
        return false;
    }
    auto* fudge = LoadLibraryA(DLLPath.data());
    if (!fudge) {
        return false;
    }
    return true;
}

SKSEPlugin(
    Name = "Fully Dynamic Game Engine";
    Version = {1, 0, 0};
    Author = "Charmed Baryon";
    Email = "charmedbaryon@elderscrolls-ng.com";
    UsesDeclarativeMessaging = false;
);
