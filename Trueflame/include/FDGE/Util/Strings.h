#pragma once

#if UNICODE || _UNICODE
#define FDGE_WIN_CHAR_TYPE wchar_t
#define FDGE_WIN_STRING_LITERAL(str) L ## str
#else
#define FDGE_WIN_CHAR_TYPE char
#define FDGE_WIN_STRING_LITERAL(str) str
#endif
#define FDGE_WIN_STRING_TYPE std::basic_string<FDGE_WIN_CHAR_TYPE>
#define FDGE_WIN_STRING_VIEW_TYPE std::basic_string_view<FDGE_WIN_CHAR_TYPE>
