#pragma once

#include <gluino/concepts.h>
#include <parallel_hashmap/phmap.h>

#include "RecursiveMutex.h"

namespace FDGE::Util {
    template <class T, class Mutex = FDGE::Util::RecursiveMutex>
    class EventSource;

    /**
     * Representation of an event listener's lifetime.
     *
     * <p>
     * When registering for an event an instance of this is returned. When the registration leaves scope and is
     * destroyed the listener is unregistered. The registration should be destroyed before the listener to ensure there
     * is no event race which can invoke a listener which no longer exists. Registrations are movable but not copyable.
     * </p>
     */
    class EventRegistration {
    public:
        inline EventRegistration() noexcept = default;

        EventRegistration(const EventRegistration&) = delete;

        inline EventRegistration(EventRegistration&& other) noexcept {
            *this = std::move(other);
        }

        inline ~EventRegistration() {
            if (_eventSource) {
                _unregister(*this);
                _eventSource = nullptr;
                _id = 0;
            }
        }

        EventRegistration& operator=(const EventRegistration&) = delete;

        inline EventRegistration& operator=(EventRegistration&& other) noexcept {
            _eventSource = other._eventSource;
            _id = other._id;
            other._eventSource = nullptr;
            other._id = 0;
            _unregister = std::move(other._unregister);
            return *this;
        }

        [[nodiscard]] inline bool IsValid() const noexcept {
            return _eventSource != nullptr;
        }

        inline bool Detach() noexcept {
            if (!_eventSource) {
                return false;
            }
            _unregister = {};
            _eventSource = nullptr;
            _id = 0;
            return true;
        }

    private:
        template <class T>
        inline EventRegistration(const EventSource<T>* eventSource, uint64_t id) noexcept;

        std::function<void(const EventRegistration&)> _unregister;
        const void* _eventSource{nullptr};
        uint64_t _id{0};

        template <class T, class Mutex>
        friend class EventSource;
    };

    namespace detail {
#pragma warning(push)
#pragma warning(disable: 4251)
        class __declspec(dllexport) EventSourceImpl {
        public:
            EventSourceImpl() noexcept;

            ~EventSourceImpl() noexcept;

            [[nodiscard]] uint64_t Register(std::function<void(const int&)> handler);

            [[nodiscard]] uint64_t LazyRegister(std::function<void(const int&)> handler);

            void CommitLazyRegistrations();

            [[nodiscard]] bool Unregister(uint64_t id);

            void Emit(const int& event);

        private:
            mutable phmap::flat_hash_map<uint64_t, std::function<void(const int&)>> _handlers;
            mutable gluino::synchronized<std::queue<std::pair<uint64_t,
                std::function<void(const int&)>>>, RecursiveMutex> _queuedRegistrations;
            mutable std::atomic_uint64_t _nextID{1};
        };
#pragma warning(pop)
    }

    /**
     * A source of events, to which event handlers can be registered.
     *
     * <p>
     * Event sources are more efficient, and more idiomatic (for C++) implementation of an event pattern than the
     * engine's <code>BSTEventSource</code>. It has optional locking (via a templated mutex type which can be set to
     * <code>gluino::null_mutex</code>), and support for shared locking as well. It is reentrant in the locked context,
     * since it enqueues operations while locked and replays them in the lock-owning thread before unlocking. It is safe
     * to use across DLL boundaries, provided that the <code>Mutex</code> template parameter is set to a mutex type that
     * can be safely used across DLL boundaries, such as those included in Trueflame.
     * </p>
     *
     * @tparam T The type of event emitted.
     * @tparam Mutex The type of mutex used to protect the event source; defaults to <code>FDGE::Util::Mutex</code>.
     *               This type should be safe to use across DLL boundaries. If the mutex type meets the requirements of
     *               the <code>gluino::shared_lockable</code> concept then it will be used as a read/write lock.
     */
    template <class T, class Mutex>
    class __declspec(dllexport) EventSource {
    public:
        inline ~EventSource() {
            delete _impl;
        }

        /**
         * Register a handler for an event.
         *
         * @tparam Handler The type of the handler.
         * @param handler  The handler.
         * @return An <code>EventRegistration</code> which owns the lifetime of the registration.
         */
        template <class Handler>
        requires(gluino::function_like<Handler, void>)
        [[nodiscard]] inline EventRegistration Listen(Handler&& handler) const {
            return Listen([handler = std::forward<Handler>(handler)](const T& event) {
                handler(event);
            });
        }

        /**
         * Register a handler for an event.
         *
         * @param handler The handler.
         * @return An <code>EventRegistration</code> which owns the lifetime of the registration.
         */
        [[nodiscard]] inline EventRegistration Listen(std::function<void(const T&)> handler) const {
            uint64_t id;
            std::unique_lock lock(_lock, std::try_to_lock_t{});
            if (lock) {
                id = _impl->Register(reinterpret_cast<std::function<void(const int&)>&>(handler));
                _impl->CommitLazyRegistrations();
            } else {
                id = _impl->LazyRegister(reinterpret_cast<std::function<void(const int&)>&>(handler));
            }
            return id ? EventRegistration(this, id) : EventRegistration();
        }

        /**
         * Register a listener that will never be unregistered.
         *
         * @tparam Handler The handler.
         */
        template <class Handler>
        requires(gluino::function_like<Handler, void>)
        void inline ListenForever(Handler&& handler) const {
            auto reg = Listen(std::forward<Handler>(handler));
            reg.Detach();
        }

        /**
         * Register a listener that will never be unregistered.
         *
         * @param handler The handler.
         */
        void inline ListenForever(std::function<void(const T&)> handler) const {
            auto reg = Listen(std::move(handler));
            reg.Detach();
        }

        /**
         * Unregister a listener.
         *
         * <p>
         * Normally this function is not explicitly called. When an <code>EventRegistration</code> leaves scope it will
         * automatically unregister.
         * </p>
         *
         * @param registration The event registration returned when the listener was registered.
         * @return <code>true</code> if the registration is valid for this listener and the listener was still listening
         *         when this function was called; otherwise <code>false</code>.
         */
        inline bool StopListening(const EventRegistration& registration) const {
            if (reinterpret_cast<const EventSource<T>*>(registration._eventSource) != this) {
                return false;
            }
            std::unique_lock lock(_lock);
            return _impl->Unregister(registration._id);
        }

    protected:
        /**
         * Emits an event.
         *
         * @param event
         */
        inline void Emit(const T& event) const {
            if constexpr (gluino::shared_lockable<Mutex>) {
                std::shared_lock lock(_lock);
                _impl->Emit(reinterpret_cast<const int&>(event));
                _impl->CommitLazyRegistrations();
            } else {
                std::unique_lock lock(_lock);
                _impl->Emit(reinterpret_cast<const int&>(event));
                _impl->CommitLazyRegistrations();
            }
        }

    private:
        mutable Mutex _lock;
        FDGE::Util::detail::EventSourceImpl* _impl{new FDGE::Util::detail::EventSourceImpl()};
    };

    template <class T>
    inline EventRegistration::EventRegistration(const EventSource<T>* eventSource, uint64_t id) noexcept
            : _eventSource(reinterpret_cast<const void*>(eventSource)), _id(id),
              _unregister([eventSource](const EventRegistration& self) { eventSource->StopListening(self); }) {
    }
}
