#pragma once

#include <gluino/concepts.h>

#include "PortableID.h"

namespace articuno::serde {
    template <::articuno::serializing_archive Archive, class Char>
    void serde(Archive& ar, RE::detail::BSFixedString<Char>& value, articuno::value_flags flags) {
        ::std::basic_string_view<Char> str = value;
        ar <=> articuno::self(str, flags);
    }

    template <::articuno::deserializing_archive Archive, class Char>
    void serde(Archive& ar, RE::detail::BSFixedString<Char>& value, articuno::value_flags flags) {
        ::std::basic_string<Char> str;
        ar <=> ::articuno::self(str, flags);
        value = str;
    }

    template <::articuno::serializing_archive Archive, class T>
    requires(::gluino::subtype_of<T, RE::BGSBaseAlias>) void serde(Archive& ar, T*& value, articuno::value_flags) {
        ar <=> articuno::kv(value->owningQuest, "quest");
        ar <=> articuno::kv(value->aliasName, "name");
    }

    template <::articuno::deserializing_archive Archive, class T>
    requires(::gluino::subtype_of<T, RE::BGSBaseAlias>) void serde(Archive& ar, T*& value, articuno::value_flags) {
        RE::TESQuest* quest;
        ar <=> articuno::kv(quest, "quest");
        RE::BSFixedString name;
        ar <=> articuno::kv(name, "name");

        if (quest) {
            for (auto* alias : quest->aliases) {
                if (alias->aliasName == name) {
                    value = alias;
                    return;
                }
            }
        }
        value = nullptr;
    }

    template <::articuno::serializing_archive Archive>
    void serde(Archive& ar, RE::BSTSmartPointer<RE::BSScript::Object>& value, articuno::value_flags) {
        ar <=> articuno::kv(value->handle, "handle");
        ar <=> articuno::kv(value->type->name, "type");
    }

    template <::articuno::deserializing_archive Archive>
    void serde(Archive& ar, RE::BSTSmartPointer<RE::BSScript::Object>& value, articuno::value_flags) {
        RE::VMHandle handle;
        RE::BSFixedString type;
        ar <=> articuno::kv(handle, "handle");
        ar <=> articuno::kv(type, "type");
        auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
        if (!vm->FindBoundObject(handle, type.c_str(), value)) {
            value.reset();
        }
    }

    template <::articuno::serializing_archive Archive>
    void serde(Archive& ar, RE::ActiveEffect*& value, articuno::value_flags) {
        FDGE::Skyrim::PortableID actorID(value->GetTargetActor());
        ar <=> articuno::kv(actorID, "target");
        ar <=> articuno::kv(value->usUniqueID, "id");
    }

    template <::articuno::deserializing_archive Archive>
    void serde(Archive& ar, RE::ActiveEffect*& value, articuno::value_flags) {
        FDGE::Skyrim::PortableID actorID;
        uint16_t id;
        ar <=> articuno::kv(actorID, "target");
        ar <=> articuno::kv(id, "id");
        auto actor = actorID.Lookup<RE::Actor>();
        if (actor) {
            for (auto* entry = actor->postUpdateDispelList; entry; entry = entry->next) {
                if (entry->activeEffect.get() && entry->activeEffect->usUniqueID == id) {
                    value = entry->activeEffect.get();
                    return;
                }
            }
        }
        value = nullptr;
    }

    template <::articuno::serializing_archive Archive>
    void serde(Archive& ar, const RE::BSFixedString& value, articuno::value_flags) {
        const auto* str = value.c_str();
        ar <=> articuno::self(str, articuno::value_flags::allow_indirection);
    }

    template <::articuno::deserializing_archive Archive>
    void serde(Archive& ar, RE::BSFixedString& value, articuno::value_flags) {
        std::string str;
        ar <=> articuno::self(str);
        value = RE::BSFixedString(str);
    }

}  // namespace articuno::serde