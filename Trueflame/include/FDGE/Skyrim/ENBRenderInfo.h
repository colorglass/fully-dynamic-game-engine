#pragma once

namespace FDGE::Skyrim {
    class ID3D11Device;

    class ID3D11DeviceContext;

    class IDXGISwapChain;

    class ENBRenderInfo {
    public:
        using size_type = DWORD;

        [[nodiscard]] inline ID3D11Device& GetDevice() const noexcept {
            return *_device;
        }

        [[nodiscard]] inline ID3D11DeviceContext& GetDeviceContext() const noexcept {
            return *_deviceContext;
        }

        [[nodiscard]] inline IDXGISwapChain& GetSwapChain() const noexcept {
            return *_swapChain;
        }

        [[nodiscard]] inline size_type GetScreenWidth() const noexcept {
            return _screenWidth;
        }

        [[nodiscard]] inline size_type GetScreenHeight() const noexcept {
            return _screenHeight;
        }

    private:
        ID3D11Device* _device;
        ID3D11DeviceContext* _deviceContext;
        IDXGISwapChain* _swapChain;
        size_type _screenWidth;
        size_type _screenHeight;
    };
}
