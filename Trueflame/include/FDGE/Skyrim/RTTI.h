#pragma once

#include <gluino/typeinfo.h>
#include <RE/Skyrim.h>
#include <REL/Relocation.h>

namespace gluino {
    template <class T>
    struct rtti<T*, std::is_base_of<RE::TESForm, T>> {
        static inline const ::std::type_info& type_info = REL::Relocation<const std::type_info&>(REL::ID(0)).get();

        template <class U = T, ::std::enable_if_t<::std::is_polymorphic_v<::std::remove_cvref_t<U>>, int> = 0>
        static inline const ::std::type_info& dynamic_type_info(U&& object) noexcept {
            return typeid(::std::forward<U>(object));
        }
    };
}
