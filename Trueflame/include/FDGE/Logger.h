#pragma once

#include <debugapi.h>
#include <fmt/format.h>
#include <fmt/xchar.h>
#include <gluino/string_cast.h>
#include <spdlog/async.h>
#include <spdlog/sinks/msvc_sink.h>
#include <spdlog/sinks/null_sink.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/win_eventlog_sink.h>

#include <source_location>

#include "Config/PluginConfig.h"
#include "Config/Proxy.h"
#include "Plugin.h"

namespace FDGE {
    namespace detail {
#ifndef __clang__
        using source_location = std::source_location;
#else
        /**
		 * A polyfill for source location support for Clang.
		 *
		 * <p>
		 * Clang-CL can use <code>source_location</code>, but not in the context of a default argument due to
		 * a bug in its support for <code>consteval</code>. This bug does not affect <code>constexpr</code> so
		 * this class uses a <code>constexpr</code> version of the typical constructor.
		 * </p>
         */
        struct source_location
        {
        public:
            static constexpr source_location current(
                const uint_least32_t a_line = __builtin_LINE(),
                const uint_least32_t a_column = __builtin_COLUMN(),
                const char* const    a_file = __builtin_FILE(),
                const char* const    a_function = __builtin_FUNCTION()) noexcept
            {
                source_location result;
                result._line = a_line;
                result._column = a_column;
                result._file = a_file;
                result._function = a_function;
                return result;
            }

            [[nodiscard]] constexpr const char* file_name() const noexcept
            {
                return _file;
            }

            [[nodiscard]] constexpr const char* function_name() const noexcept
            {
                return _function;
            }

            [[nodiscard]] constexpr uint_least32_t line() const noexcept
            {
                return _line;
            }

            [[nodiscard]] constexpr uint_least32_t column() const noexcept
            {
                return _column;
            }

        private:
            source_location() = default;

            uint_least32_t _line{};
            uint_least32_t _column{};
            const char*    _file = "";
            const char*    _function = "";
        };
#endif
    }

    /**
     * Implements a customizable and powerful logging system with dynamic configuration updates.
     */
    class Logger {
    public:
        /**
         * Initializes the logger with configuration from a proxy.
         *
         * <p>
         * This will load data from the configuration managed by the proxy. It also registers as a listener so that it
         * can refresh logging configuration when the proxy signals an update. The name of the logging outputs will be
         * based on the plugin name stored in the Trueflame plugin declaration, which is required for this call.
         * </p>
         *
         * @tparam T The type of data stored in the proxy; must conform to the <code>DebugConfig</code> class from
         * Trueflame.
         * @param proxy The proxy with the the data to load.
         */
        template <class T>
        static void Initialize(Config::Proxy<T>& proxy) {
            auto plugin = detail::Plugin::Lookup();
            if (!plugin) {
                throw std::logic_error(
                    "Initializing the logger without an explicit plugin name requires the "
                    "SKSEPlugin_Version structure to be present and include a plugin name.");
            }
            auto* name = reinterpret_cast<const char*>(reinterpret_cast<uintptr_t>(plugin.get()) + 0x8);
            if (name[0] == '\0') {
                throw std::logic_error(
                    "Initializing the logger without an explicit plugin name requires the "
                    "SKSEPlugin_Version structure to include a plugin name.");
            }
            auto pluginName = gluino::claim(gluino::string_cast<FDGE_WIN_CHAR_TYPE>(name));
            _instance.reset(new Logger(proxy, pluginName));
        }

        /**
         * Initializes the logger with configuration from a proxy.
         *
         * <p>
         * This will load data from the configuration managed by the proxy. It also registers as a listener so that it
         * can refresh logging configuration when the proxy signals an update. The name of the logging outputs must be
         * given explicitly in this call, but this allows the use of non-Trueflame plugin declarations..
         * </p>
         *
         * @tparam T The type of data stored in the proxy; must conform to the <code>DebugConfig</code> class from
         * Trueflame.
         * @param proxy The proxy with the the data to load.
         * @param pluginName The name to assume for the plugin when generating logging outputs.
         */
        template <class T>
        static void Initialize(Config::Proxy<T>& proxy, FDGE_WIN_STRING_VIEW_TYPE pluginName) {
            _instance.reset(new Logger(proxy, pluginName));
        }

        template <class Char, class... Args>
        struct Log {
            inline explicit Log(spdlog::level::level_enum level, fmt::basic_format_string<Char, Args...> format,
                                Args&&... args, detail::source_location location = detail::source_location::current()) {
                LogMessage(level, location, format, std::forward<Args>(args)...);
            }

            inline explicit Log(std::basic_string_view<Char> file, std::basic_string_view<Char> function, uint32_t line,
                                spdlog::level::level_enum level, fmt::basic_format_string<Char, Args...> format,
                                Args&&... args) {
                LogMessage(file, function, line, level, format, std::forward<Args>(args)...);
            }

            inline explicit Log(spdlog::level::level_enum level, fmt::basic_runtime<Char> format, Args&&... args,
                                detail::source_location location = detail::source_location::current()) {
                LogMessage(level, location, format, std::forward<Args>(args)...);
            }

            inline explicit Log(std::basic_string_view<Char> file, std::basic_string_view<Char> function, uint32_t line,
                                spdlog::level::level_enum level, fmt::basic_runtime<Char> format, Args&&... args) {
                LogMessage(file, function, line, level, format, std::forward<Args>(args)...);
            }

            inline explicit Log(spdlog::level::level_enum level, std::basic_string_view<Char> message,
                                detail::source_location location = detail::source_location::current()) {
                LogMessage(level, location, fmt::runtime(message));
            }

            inline explicit Log(std::basic_string_view<Char> file, std::basic_string_view<Char> function, uint32_t line,
                                spdlog::level::level_enum level, std::basic_string_view<Char> message) {
                LogMessage(file, function, line, level, fmt::runtime(message));
            }

            inline explicit Log(spdlog::level::level_enum level, const Char* message,
                                detail::source_location location = detail::source_location::current()) {
                LogMessage(level, location, fmt::runtime(message));
            }

            inline explicit Log(std::basic_string_view<Char> file, std::basic_string_view<Char> function, uint32_t line,
                                spdlog::level::level_enum level, const Char* message) {
                LogMessage(file, function, line, level, fmt::runtime(message));
            }
        };

        template <class Char, class... Args>
        Log(spdlog::level::level_enum, fmt::basic_format_string<Char, Args...>, Args&&...) -> Log<Char, Args...>;

        template <class Char, class... Args>
        Log(std::basic_string_view<Char>, std::basic_string_view<Char>, uint32_t, spdlog::level::level_enum,
            fmt::basic_format_string<Char, Args...>, Args&&...) -> Log<Char, Args...>;

        template <class Char, class... Args>
        Log(spdlog::level::level_enum, fmt::basic_runtime<Char>, Args&&...) -> Log<Char, Args...>;

        template <class Char, class... Args>
        Log(std::basic_string_view<Char>, std::basic_string_view<Char>, uint32_t, spdlog::level::level_enum,
            fmt::basic_runtime<Char>, Args&&...) -> Log<Char, Args...>;

        template <class Char, class... Args>
        Log(spdlog::level::level_enum, std::basic_string_view<Char>, Args&&...) -> Log<Char, Args...>;

        template <class Char, class... Args>
        Log(std::basic_string_view<Char>, std::basic_string_view<Char>, uint32_t, spdlog::level::level_enum,
            std::basic_string_view<Char>, Args&&...) -> Log<Char, Args...>;

        template <class Char, class... Args>
        Log(spdlog::level::level_enum, const Char*, Args&&...) -> Log<Char, Args...>;

        template <class Char, class... Args>
        Log(std::basic_string_view<Char>, std::basic_string_view<Char>, uint32_t, spdlog::level::level_enum,
            const Char*, Args&&...) -> Log<Char, Args...>;

#define FDGE_DEFINE_LOG_LEVEL(name, logLevel)                                                               \
    template <class Char = char, class... Args>                                                             \
    struct name {                                                                                           \
        name() = delete;                                                                                    \
        name(const name&) = delete;                                                                         \
        name(name&&) = delete;                                                                              \
                                                                                                            \
        inline explicit name(fmt::basic_format_string<Char, Args...> format, Args&&... args,                \
                             detail::source_location location = detail::source_location::current()) { \
            LogMessage(spdlog::level::logLevel, location, format, std::forward<Args>(args)...);             \
        }                                                                                                   \
                                                                                                            \
        inline explicit name(fmt::basic_runtime<Char> format, Args&&... args,                               \
                             detail::source_location location = detail::source_location::current()) { \
            LogMessage(spdlog::level::logLevel, location, format, std::forward<Args>(args)...);             \
        }                                                                                                   \
                                                                                                            \
        inline explicit name(std::basic_string_view<Char> message,                                          \
                             detail::source_location location = detail::source_location::current()) { \
            LogMessage(spdlog::level::logLevel, location, fmt::runtime(message));                           \
        }                                                                                                   \
                                                                                                            \
        inline explicit name(const Char* message,                                                           \
                             detail::source_location location = detail::source_location::current()) { \
            LogMessage(spdlog::level::logLevel, location, fmt::runtime(message));                           \
        }                                                                                                   \
    };                                                                                                      \
                                                                                                            \
    template <class Char = char, class... Args>                                                             \
    name(fmt::basic_format_string<Char, Args...>, Args&&...) -> name<Char, Args...>;                        \
                                                                                                            \
    template <class Char = char, class... Args>                                                             \
    name(fmt::basic_runtime<Char>, Args&&...) -> name<Char, Args...>;                                       \
                                                                                                            \
    template <class Char = char, class... Args>                                                             \
    name(std::basic_string_view<Char>, Args&&...) -> name<Char, Args...>;                                   \
                                                                                                            \
    template <class Char = char, class... Args>                                                             \
    name(const Char*, Args&&...) -> name<Char, Args...>;

        FDGE_DEFINE_LOG_LEVEL(Critical, critical)
        FDGE_DEFINE_LOG_LEVEL(Error, err)
        FDGE_DEFINE_LOG_LEVEL(Warn, warn)
        FDGE_DEFINE_LOG_LEVEL(Info, info)
        FDGE_DEFINE_LOG_LEVEL(Debug, debug)
        FDGE_DEFINE_LOG_LEVEL(Trace, trace)

        template <class Exception = std::runtime_error, class Char = char, class... Args>
        struct Fatal {
            Fatal() = delete;
            Fatal(const Fatal&) = delete;
            Fatal(Fatal&&) = delete;

            inline explicit Fatal(fmt::basic_format_string<Char, Args...> format, Args&&... args,
                                  detail::source_location location = detail::source_location::current()) {
                Terminate(LogMessage(spdlog::level::critical, location, format, std::forward<Args>(args)...), location);
            }

            inline explicit Fatal(fmt::basic_runtime<Char> format, Args&&... args,
                                  detail::source_location location = detail::source_location::current()) {
                Terminate(LogMessage(spdlog::level::critical, location, format, std::forward<Args>(args)...), location);
            }

            inline explicit Fatal(std::basic_string_view<Char> message,
                                  detail::source_location location = detail::source_location::current()) {
                Critical<Char>(message, location);
                std::string msg = gluino::claim(gluino::string_cast<char>(message));
                Terminate(msg, location);
            }

            inline explicit Fatal(const Char* message,
                                  detail::source_location location = detail::source_location::current()) {
                Critical<Char>(message, location);
                std::string msg = gluino::claim(gluino::string_cast<char>(message));
                Terminate(msg, location);
            }

        private:
            template <class S>
            void Terminate(S&& message, detail::source_location location) {
#if defined(ENABLE_COMMONLIBSSE_TESTING) || defined(ENABLE_FDGE_TESTING)

                throw Exception(message);
#else
                const auto body = [&]() -> std::basic_string<TCHAR> {
                    const std::filesystem::path p = location.file_name();
                    auto filename = p.lexically_normal().generic_string<TCHAR>();
                    const std::basic_regex<TCHAR> r{TEXT("(?:^|[\\\\\\/])(?:include|src)[\\\\\\/](.*)$")};
                    std::match_results<std::basic_string<TCHAR>::const_iterator> matches;
                    if (std::regex_search(filename, matches, r)) {
                        filename = matches[1].str();
                    }
                    return fmt::format(TEXT("{}({}): {}"), filename, location.line(),
                                       gluino::string_cast<TCHAR>(message));
                }();

                const auto caption = []() -> std::basic_string<TCHAR> {
                    const auto maxPath = SKSE::WinAPI::GetMaxPath();
                    std::vector<TCHAR> buf;
                    buf.reserve(maxPath);
                    buf.resize(maxPath / 2);
                    std::uint32_t result = 0;
                    do {
                        buf.resize(buf.size() * 2);
                        result = GetModuleFileNameW(reinterpret_cast<HMODULE>(SKSE::WinAPI::GetCurrentModule()),
                                                    buf.data(), static_cast<std::uint32_t>(buf.size()));
                    } while (result && result == buf.size() &&
                             buf.size() <= (std::numeric_limits<std::uint32_t>::max)());

                    if (result && result != buf.size()) {
                        std::filesystem::path p(buf.begin(), buf.begin() + result);
                        return p.filename().native();
                    }
                    return L"";
                }();

                MessageBox(nullptr, body.c_str(), (caption.empty() ? nullptr : caption.c_str()), 0);
                SKSE::WinAPI::TerminateProcess(SKSE::WinAPI::GetCurrentProcess(), EXIT_FAILURE);
#endif
            }
        };

        template <class Exception = std::runtime_error, class Char = char, class... Args>
        Fatal(fmt::basic_format_string<Char, Args...>, Args&&...) -> Fatal<Exception, Char, Args...>;

        template <class Exception = std::runtime_error, class Char = char, class... Args>
        Fatal(fmt::basic_runtime<Char>, Args&&...) -> Fatal<Exception, Char, Args...>;

        template <class Exception = std::runtime_error, class Char = char, class... Args>
        Fatal(std::basic_string_view<Char>, Args&&...) -> Fatal<Exception, Char, Args...>;

        template <class Exception = std::runtime_error, class Char = char, class... Args>
        Fatal(const Char*, Args&&...) -> Fatal<Exception, Char, Args...>;

    private:
        template <class T>
        inline explicit Logger(Config::Proxy<T>& proxy, FDGE_WIN_STRING_VIEW_TYPE pluginName)
            : _pluginName(pluginName.data()) {
            proxy.ListenForever([this](const Config::DataRefreshedEvent&) { _updateHandler(true); });
            _updateHandler = [&](bool reinitializing) { Update(proxy, reinitializing); };

            _updateHandler(false);
        }

        template <class Char, class... Args>
        static std::basic_string<Char> LogMessage(spdlog::level::level_enum level, detail::source_location location,
                                                  const fmt::basic_format_string<Char, Args...>& format,
                                                  Args&&... args) noexcept {
            return LogMessage(location.file_name(), location.function_name(), location.line(), level, format,
                              std::forward<Args>(args)...);
        }

        template <class Char, class... Args>
        static std::basic_string<Char> LogMessage(spdlog::level::level_enum level, detail::source_location location,
                                                  const fmt::basic_runtime<Char>& format, Args&&... args) noexcept {
            return LogMessage(location.file_name(), location.function_name(), location.line(), level, format,
                              std::forward<Args>(args)...);
        }

        template <class Char, class... Args>
        static std::basic_string<Char> LogMessage(std::string_view file, std::string_view function, uint32_t line,
                                                  spdlog::level::level_enum level,
                                                  const fmt::basic_format_string<Char, Args...>& format,
                                                  Args&&... args) noexcept {
            if (!WouldLog(level)) {
                return {};
            }
            std::basic_string<Char> msg;
            if constexpr (std::same_as<Char, char>) {
                msg = fmt::format(format, std::forward<Args>(args)...);
            } else {
                auto view = static_cast<fmt::basic_string_view<Char>>(format);
                const auto& vargs = fmt::make_args_checked<Args...>(view, std::forward<Args>(args)...);
                msg = vformat(view, vargs);
            }
            LogMessage<Char>(file, function, line, level, msg);
            return std::move(msg);
        }

        template <class Char, class... Args>
        static std::basic_string<Char> LogMessage(std::string_view file, std::string_view function, uint32_t line,
                                                  spdlog::level::level_enum level,
                                                  const fmt::basic_runtime<Char>& format, Args&&... args) noexcept {
            if (!WouldLog(level)) {
                return {};
            }
            std::basic_string<Char> msg = fmt::format(format, std::forward<Args>(args)...);
            LogMessage<Char>(file, function, line, level, msg);
            return std::move(msg);
        }

        template <class Char>
        static void LogMessage(std::string_view file, std::string_view function, uint32_t line,
                               spdlog::level::level_enum level, std::basic_string_view<Char> msg) noexcept {
            decltype(auto) logLine = gluino::string_cast<char>(msg);
            spdlog::source_loc loc{file.data(), static_cast<int>(line), function.data()};
            if (_instance->_debuggerLogger) {
                _instance->_debuggerLogger->log(loc, level, logLine.data());
            }
            if (_instance->_fileLogger) {
                _instance->_fileLogger->log(loc, level, logLine.data());
            }
            if (_instance->_eventViewerLogger) {
                _instance->_eventViewerLogger->log(loc, level, logLine.data());
            }
        }

        [[nodiscard]] static inline bool WouldLog(spdlog::level::level_enum level) noexcept {
            return _instance && ((_instance->_debuggerLogger && level >= _instance->_debuggerLogger->level()) ||
                           (_instance->_fileLogger && level >= _instance->_fileLogger->level()) ||
                           (_instance->_eventViewerLogger && level >= _instance->_eventViewerLogger->level()));
        }

        template <class T>
        void Update(Config::Proxy<T>& proxy, bool reinitializing) {
            const auto& debugConfig = proxy.Get()->GetDebug();

            if (debugConfig.IsAsyncLogging() && debugConfig.GetLoggingQueueSize() > 0 &&
                debugConfig.GetLoggingThreadCount() > 0) {
                spdlog::init_thread_pool(debugConfig.GetLoggingQueueSize(), debugConfig.GetLoggingThreadCount());
            } else {
                spdlog::details::registry::instance().set_tp(nullptr);
            }
            spdlog::flush_every(debugConfig.GetFlushPeriod());

            // Flush pending log entries after dropping loggers.
            spdlog::drop_all();
            if (_debuggerLogger) {
                _debuggerLogger->flush();
            }
            if (_fileLogger) {
                _fileLogger->flush();
            }
            if (_eventViewerLogger) {
                _eventViewerLogger->flush();
            }

            _nullLogger =
                std::make_shared<spdlog::logger>("nullLogger", std::make_shared<spdlog::sinks::null_sink_mt>());
            spdlog::set_default_logger(_nullLogger);

            // Setup debugger logging.
            if (debugConfig.GetDebuggerLogger().IsEnabled() &&
                (!debugConfig.GetDebuggerLogger().IsRequireDebuggerPresent() || IsDebuggerPresent())) {
                if (debugConfig.IsAsyncLogging()) {
                    if (debugConfig.GetDebuggerLogger().IsNonBlocking()) {
                        _debuggerLogger = spdlog::create_async_nb<spdlog::sinks::msvc_sink_mt>("debuggerLogger");
                    } else {
                        _debuggerLogger = spdlog::create_async<spdlog::sinks::msvc_sink_mt>("debuggerLogger");
                    }
                } else {
                    _debuggerLogger = std::make_shared<spdlog::logger>("debuggerLogger",
                                                                       std::make_shared<spdlog::sinks::msvc_sink_mt>());
                }
                _debuggerLogger->set_pattern(debugConfig.GetDebuggerLogger().GetLogPattern());
                _debuggerLogger->set_level(debugConfig.GetDebuggerLogger().GetLogLevel());
                _debuggerLogger->flush_on(debugConfig.GetDebuggerLogger().GetFlushLevel());
                spdlog::register_logger(_debuggerLogger);
                if (debugConfig.GetDefaultLogger() == Config::DefaultLogger::debuggerLogger) {
                    spdlog::set_default_logger(_debuggerLogger);
                }
            } else {
                _debuggerLogger = nullptr;
            }

            // Setup event viewer logging.
            if (debugConfig.GetEventViewerLogger().IsEnabled()) {
                auto source = gluino::claim(gluino::string_cast<char>(_pluginName));
                if (debugConfig.IsAsyncLogging()) {
                    if (debugConfig.GetEventViewerLogger().IsNonBlocking()) {
                        _eventViewerLogger =
                            spdlog::create_async_nb<spdlog::sinks::win_eventlog_sink_mt>("eventViewerLogger", source);
                    } else {
                        _debuggerLogger =
                            spdlog::create_async<spdlog::sinks::win_eventlog_sink_mt>("eventViewerLogger", source);
                    }
                } else {
                    _eventViewerLogger = std::make_shared<spdlog::logger>(
                        "eventViewerLogger", std::make_shared<spdlog::sinks::win_eventlog_sink_mt>(source));
                }
                _eventViewerLogger->set_pattern(debugConfig.GetEventViewerLogger().GetLogPattern());
                _eventViewerLogger->set_level(debugConfig.GetEventViewerLogger().GetLogLevel());
                _eventViewerLogger->flush_on(debugConfig.GetEventViewerLogger().GetFlushLevel());
                spdlog::register_logger(_eventViewerLogger);
                if (debugConfig.GetDefaultLogger() == Config::DefaultLogger::eventViewerLogger) {
                    spdlog::set_default_logger(_eventViewerLogger);
                }
            } else {
                _eventViewerLogger = nullptr;
            }

            // Setup file logging.
            _logFile = GetLogFilePath(debugConfig.GetFileLogger().GetLogFile());
            if (debugConfig.GetFileLogger().IsEnabled() && !_logFile.empty()) {
                auto logPathStr = gluino::claim(gluino::string_cast<char>(_logFile));
                if (debugConfig.IsAsyncLogging()) {
                    if (debugConfig.GetDebuggerLogger().IsNonBlocking()) {
                        _fileLogger = spdlog::create_async_nb<spdlog::sinks::rotating_file_sink_mt>(
                            "fileLogger", logPathStr, debugConfig.GetFileLogger().GetMaximumFileSize().ToBytes(),
                            debugConfig.GetFileLogger().GetMaximumFiles(),
                            !(debugConfig.GetFileLogger().IsAppendMode() || reinitializing));
                    } else {
                        _fileLogger = spdlog::create_async<spdlog::sinks::rotating_file_sink_mt>(
                            "fileLogger", logPathStr, debugConfig.GetFileLogger().GetMaximumFileSize().ToBytes(),
                            debugConfig.GetFileLogger().GetMaximumFiles(),
                            !(debugConfig.GetFileLogger().IsAppendMode() || reinitializing));
                    }
                } else {
                    _fileLogger = spdlog::rotating_logger_mt(
                        "fileLogger", logPathStr, debugConfig.GetFileLogger().GetMaximumFileSize().ToBytes(),
                        debugConfig.GetFileLogger().GetMaximumFiles(),
                        !(debugConfig.GetFileLogger().IsAppendMode() || reinitializing));
                }
                _fileLogger->set_pattern(debugConfig.GetFileLogger().GetLogPattern());
                _fileLogger->set_level(debugConfig.GetFileLogger().GetLogLevel());
                _fileLogger->flush_on(debugConfig.GetFileLogger().GetFlushLevel());
                if (debugConfig.GetDefaultLogger() == Config::DefaultLogger::fileLogger) {
                    spdlog::set_default_logger(_fileLogger);
                }
            } else {
                _fileLogger = nullptr;
            }
        }

        [[nodiscard]] FDGE_WIN_STRING_TYPE GetLogFilePath(const std::filesystem::path& logFile) const {
            FDGE_WIN_STRING_TYPE logPath = logFile.template generic_string<FDGE_WIN_CHAR_TYPE>();
            if (logPath.empty()) {
                auto maybeLogPath = SKSE::log::log_directory();
                if (maybeLogPath.has_value()) {
                    logPath = maybeLogPath.value().generic_string<FDGE_WIN_CHAR_TYPE>();
                } else {
                    throw std::logic_error("Unable to determine log file location.");
                }
                logPath += FDGE_WIN_STRING_LITERAL('/');
                logPath += _pluginName;
                logPath += FDGE_WIN_STRING_LITERAL(".log");
            }
            return logPath;
        }

        static inline std::unique_ptr<Logger> _instance;
        std::function<void(bool)> _updateHandler;
        FDGE_WIN_STRING_TYPE _pluginName;
        FDGE_WIN_STRING_TYPE _logFile;
        std::shared_ptr<spdlog::logger> _nullLogger;
        std::shared_ptr<spdlog::logger> _fileLogger;
        std::shared_ptr<spdlog::logger> _debuggerLogger;
        std::shared_ptr<spdlog::logger> _eventViewerLogger;
    };
}  // namespace FDGE
