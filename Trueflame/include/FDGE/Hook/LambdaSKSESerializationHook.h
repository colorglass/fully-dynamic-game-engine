#pragma once

#include "SKSESerializationHook.h"

namespace FDGE::Hook {
    class LambdaSKSESerializationHook : public SKSESerializationHook {
    public:
        inline LambdaSKSESerializationHook(std::string_view name, std::function<void(std::ostream&)>&& saver,
                                    std::function<void(std::istream&)>&& loader,
                                    std::function<void()> reverter = []() {})
                                    : SKSESerializationHook(name), _saveCallback(std::move(saver)),
                                    _loadCallback(std::move(loader)), _revertCallback(std::move(reverter)) {
        }

    protected:
        void OnRevert() override;

        void OnGameSaved(std::ostream& out) override;

        void OnGameLoaded(std::istream& in) override;

    private:
        std::function<void(std::ostream&)> _saveCallback;
        std::function<void(std::istream&)> _loadCallback;
        std::function<void()> _revertCallback;;
    };
}
