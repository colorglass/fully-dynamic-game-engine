#pragma once

#include "LoggerConfig.h"
#include "../Util/DataSize.h"

namespace FDGE::Config {
    class FileLoggerConfig : public LoggerConfig {
    public:
        [[nodiscard]] inline const std::filesystem::path& GetLogFile() const noexcept {
            return _logFile;
        }

        inline void SetLogFile(std::filesystem::path logFile) {
            _logFile = std::move(logFile);
        }

        [[nodiscard]] inline bool IsAppendMode() const noexcept {
            return _appendMode;
        }

        inline void SetAppendMode(bool appendMode) noexcept {
            _appendMode = appendMode;
        }

        [[nodiscard]] inline uint32_t GetMaximumFiles() const noexcept {
            return _maximumFiles;
        }

        inline void SetMaximumFiles(uint32_t maximumFiles) noexcept {
            _maximumFiles = maximumFiles;
        }

        [[nodiscard]] inline Util::DataSize GetMaximumFileSize() const noexcept {
            return _maximumFileSize;
        }

        inline void SetMaximumFileSize(Util::DataSize maximumFileSize) noexcept {
            _maximumFileSize = maximumFileSize;
        }

    protected:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_logFile, "logFile");
            ar <=> articuno::kv(_appendMode, "appendMode");
            ar <=> articuno::kv(_maximumFiles, "maximumFiles");
            ar <=> articuno::kv(_maximumFileSize, "maximumFileSize");
            articuno_super(LoggerConfig, ar, flags);
        }

    private:
        std::filesystem::path _logFile;
        bool _appendMode{false};
        uint32_t _maximumFiles{5};
        Util::DataSize _maximumFileSize{1024 * 1024 * 10};

        friend class articuno::access;
    };
}
