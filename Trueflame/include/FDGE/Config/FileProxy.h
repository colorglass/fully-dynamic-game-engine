#pragma once

#include <gluino/string_cast.h>

#include "Proxy.h"
#include "../Logger.h"
#include "../Util/FilesystemWatcher.h"

namespace FDGE::Config {
    template <class T, class Provider = ArticunoProxyProvider<>>
    class FileProxy : public Proxy<T> {
    public:
        using provider_type = Provider;

        explicit FileProxy(std::filesystem::path path, typename Provider::file_type type = {},
                           ProxyOptions options = {}) : _path(std::move(path)), _type(type), Proxy<T>(options) {
        }

        void StartWatching() const override {
            auto parentPath = _path.parent_path();
            _watcher = std::make_unique<Util::FilesystemWatcher>(
                    parentPath,
                    [this](const auto& path, auto event) {
                        if (_path.filename() == path.filename()) {
                            Proxy<T>::DoRefresh();
                        }
                    });
        }

        void StopWatching() const override {
            _watcher.release();
        }

        void Save() const override {
            auto config = Proxy<T>::_data.load();
            // TODO:
        }

        [[nodiscard]] inline const std::filesystem::path& GetFilePath() const noexcept {
            return _path;
        }

        [[nodiscard]] inline typename Provider::file_type GetFileType() const noexcept {
            return _type;
        }

    protected:
        void Refresh(bool isReloading) const override {
            std::basic_ifstream<typename Provider::char_type, typename Provider::traits_type> input(_path);
            if (std::filesystem::exists(_path) &&
                (std::filesystem::is_regular_file(_path) || std::filesystem::is_symlink(_path)) && !input.bad()) {
                auto data = std::make_shared<T>();
                Provider::template Read(input, _type, *data);
                Proxy<T>::_data = std::move(data);
            } else {
                if (isReloading) {
                    if (Proxy<T>::GetOptions().any(ProxyOptions::RequireContinuously)) {
                        throw std::runtime_error(fmt::format("Unable to find required file '{}'.", _path.string()));
                    }
                } else {
                    if (Proxy<T>::GetOptions().any(ProxyOptions::RequireAtInitialization,
                                                   ProxyOptions::RequireContinuously)) {
                        throw std::runtime_error(fmt::format("Unable to find required file '{}'.", _path.string()));
                    }
                }
            }
        }

    private:
        typename Provider::file_type _type;
        std::filesystem::path _path;
        mutable std::unique_ptr<Util::FilesystemWatcher> _watcher;
    };
}
