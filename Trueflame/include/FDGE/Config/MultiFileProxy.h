#pragma once

#include <gluino/concepts.h>

#include "Proxy.h"
#include "../Util/FilesystemWatcher.h"

namespace FDGE::Config {
    template <class T>
    concept FileSpec = gluino::iterable<T> && std::copy_constructible<T> &&
            requires (T& value, const std::filesystem::path& path) {
                { *value.begin() } -> std::convertible_to<std::filesystem::path>;
                { value(path) } -> std::convertible_to<bool>;
            };

    template <class T, class Provider = ArticunoProxyProvider<>>
    class MultiFileProxy : public Proxy<std::vector<T>> {
    public:
        using provider_type = Provider;

        template <FileSpec F>
        explicit MultiFileProxy(std::filesystem::path root, F&& fileSpec) {
            Initialize(std::move(root), std::forward<F>(fileSpec));
        }

        void StartWatching() const override {
            _watcher = std::make_unique<Util::FilesystemWatcher>(
                    _root,
                    [this](const auto& path, auto event) {
                        if (_filePredicate(path)) {
                            Proxy<T>::DoRefresh();
                        }
                    });
        }

        void StopWatching() const override {
            _watcher.release();
        }

        void Save() const override {
            auto config = Proxy<T>::_data.load();
            // TODO:
        }

        [[nodiscard]] inline const std::filesystem::path& GetRoot() const noexcept {
            return _root;
        }

    protected:
        void Refresh(bool isReloading) const override {
            auto results = std::make_shared<std::vector<T>>();
            _iterator([&](const std::filesystem::path& path) {
                std::basic_ifstream<typename Provider::char_type, typename Provider::traits_type> input(path);
                if (input.good()) {
                    auto type = Provider::GetType(path);
                    auto& data = results->emplace_back();
                    Provider::Read(input, *data);
                }
            });

        }

    private:
        template <FileSpec F>
        void Initialize(std::filesystem::path root, F&& fileSpec) {
            _root = std::move(root);
            _filePredicate = [fileSpec](const std::filesystem::path& path) {
                return fileSpec(path);
            };
            _iterator = [fileSpec](std::function<const std::filesystem::path&>&& callback) {
                for (decltype(auto) path : fileSpec) {
                    callback(path);
                }
            };
        }

        std::filesystem::path _root;
        std::function<bool(const std::filesystem::path&)> _filePredicate;
        std::function<void(std::function<const std::filesystem::path&>&&)> _iterator;
        mutable std::unique_ptr<Util::FilesystemWatcher> _watcher;
    };
}
