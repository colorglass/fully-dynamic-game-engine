#pragma once

#include <articuno/articuno.h>
#include <articuno/types/auto.h>
#include <spdlog/spdlog.h>

namespace FDGE::Config {
    class LoggerConfig {
    public:
        using log_level_type = spdlog::level::level_enum;

        [[nodiscard]] inline bool IsEnabled() const noexcept {
            return _enabled;
        }

        inline void SetEnabled(bool enabled) noexcept {
            _enabled = enabled;
        }

        [[nodiscard]] inline bool IsNonBlocking() const noexcept {
            return _nonBlocking;
        }

        inline void SetNonBlocking(bool nonBlocking) noexcept {
            _nonBlocking = nonBlocking;
        }

        [[nodiscard]] inline log_level_type GetLogLevel() const noexcept {
            return _logLevel;
        }

        inline void SetLogLevel(log_level_type logLevel) noexcept {
            _logLevel = logLevel;
        }

        [[nodiscard]] inline log_level_type GetFlushLevel() const noexcept {
            return _flushLevel;
        }

        inline void SetFlushLevel(log_level_type flushLevel) noexcept {
            _flushLevel = flushLevel;
        }

        [[nodiscard]] inline const std::string& GetLogPattern() const noexcept {
            return _logPattern;
        }

        inline void SetLogPattern(std::string_view logPattern) {
            _logPattern = logPattern.data();
        }

    protected:
        inline explicit LoggerConfig(bool defaultEnabled = true, log_level_type defaultLogLevel = log_level_type::info,
                            log_level_type defaultFlushLevel = log_level_type::trace) noexcept
                            : _enabled(defaultEnabled), _logLevel(defaultLogLevel), _flushLevel(defaultFlushLevel) {
        }

        articuno_serde(ar) {
            ar <=> articuno::kv(_enabled, "enabled");
            ar <=> articuno::kv(_nonBlocking, "nonBlocking");
            ar <=> articuno::kv(_logLevel, "logLevel");
            ar <=> articuno::kv(_flushLevel, "flushLevel");
            ar <=> articuno::kv(_logPattern, "logPattern");
        }

    private:
        bool _enabled;
        bool _nonBlocking;
        log_level_type _logLevel;
        log_level_type _flushLevel;
        std::string _logPattern{"[%Y-%m-%d %H:%M:%S.%e] [%n] [%l] [%t] [%s:%#] %v"};

        friend class articuno::access;
    };
}

namespace articuno::serde {
    template <::articuno::serializing_archive Archive>
    void serde(Archive& ar, spdlog::level::level_enum& value, articuno::value_flags) {
        ar <=> articuno::self(spdlog::level::to_string_view(value));
    }

    template <::articuno::deserializing_archive Archive>
    void serde(Archive& ar, spdlog::level::level_enum& value, articuno::value_flags) {
        std::string str;
        if ((ar <=> ::articuno::self(str))) {
            std::transform(str.begin(), str.end(), str.begin(),
                           [](char in) { return static_cast<char>(std::tolower(in)); });
            value = spdlog::level::from_str(str);
        }
    }
}
