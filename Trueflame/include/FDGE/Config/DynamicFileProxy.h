#pragma once

#include "Proxy.h"
#include "../Util/FilesystemWatcher.h"

namespace FDGE::Config {
    template <class T, class Provider = ArticunoProxyProvider<>>
    class DynamicFileProxy : public Proxy<T> {
    public:
        inline explicit DynamicFileProxy(std::filesystem::path baseFilePath, ProxyOptions options = {})
                : _path(std::move(baseFilePath)), Proxy<T>(options) {
        }

        void StartWatching() const override {
            auto parentPath = _path.parent_path();
            _watcher = std::make_unique<Util::FilesystemWatcher>(
                    parentPath,
                    [this, baseFile = _path.filename()](auto& file, auto) {
                        if (std::filesystem::path(file).stem() == baseFile) {
                            Proxy<T>::DoRefresh();
                        }
                    });
        }

        void StopWatching() const override {
            _watcher.release();
        }

        void Save() const override {
            auto config = Proxy<T>::_data.load();
            // TODO:
        }

        [[nodiscard]] inline const std::filesystem::path& GetBaseFilePath() const noexcept {
            return _path;
        }

    private:
        void Refresh(bool isReloading) const override {
            for (auto& extension: Provider::GetSupportedExtensions()) {
                std::filesystem::path path = _path;
                path += ".";
                path += extension.first;
                if (std::filesystem::exists(path) &&
                    (std::filesystem::is_regular_file(path) || std::filesystem::is_symlink(path))) {
                    if (Refresh(isReloading, path, extension.second)) {
                        return;
                    }
                    break;
                }
            }
            if (isReloading) {
                if (Proxy<T>::GetOptions().any(ProxyOptions::RequireContinuously)) {
                    throw std::runtime_error(fmt::format("Unable to find required file with base name '{}'.",
                                                         _path.string()));
                }
            } else {
                if (Proxy<T>::GetOptions().any(ProxyOptions::RequireAtInitialization,
                                               ProxyOptions::RequireContinuously)) {
                    throw std::runtime_error(fmt::format("Unable to find required file wuth base name '{}'.",
                                                         _path.string()));
                }
            }
        }

        [[nodiscard]] bool
        Refresh(bool, const std::filesystem::path& path, typename Provider::file_type type) const {
            std::basic_ifstream<typename Provider::char_type, typename Provider::traits_type> input(path);
            if (!input.bad()) {
                auto data = std::make_shared<T>();
                Provider::template Read(input, type, *data);
                Proxy<T>::_data = std::move(data);
                return true;
            }
            return false;
        }

        std::filesystem::path _path;
        mutable std::unique_ptr<Util::FilesystemWatcher> _watcher;
    };
}
