#pragma once

#include "LoggerConfig.h"

namespace FDGE::Config {
    class DebuggerLoggerConfig : public LoggerConfig {
    public:
        inline DebuggerLoggerConfig() noexcept
                : LoggerConfig(true, log_level_type::debug) {
        }

        [[nodiscard]] inline bool IsRequireDebuggerPresent() const noexcept {
            return _requireDebuggerPresent;
        }

        inline void SetRequireDebuggerPresent(bool requireDebuggerPresent) noexcept {
            _requireDebuggerPresent = requireDebuggerPresent;
        }

    protected:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_requireDebuggerPresent, "requireDebuggerPresent");
            articuno_super(LoggerConfig, ar, flags);
        }

    private:
        bool _requireDebuggerPresent{false};

        friend class articuno::access;
    };
}
