#pragma once

#include <articuno/articuno.h>

#include "DebugConfig.h"

namespace FDGE::Config {
    template <class T>
    class PluginConfig {
    public:
        using debug_config_type = T;

        [[nodiscard]] inline debug_config_type& GetDebug() noexcept {
            return _debug;
        }

        [[nodiscard]] inline const debug_config_type& GetDebug() const noexcept {
            return _debug;
        }

    protected:
        articuno_serde(ar) {
            ar <=> articuno::kv(_debug, "debug");
        }

    protected:
        PluginConfig() = default;

        explicit PluginConfig(debug_config_type& debug) : _debug(debug) {
        }

        explicit PluginConfig(debug_config_type&& debug) : _debug(std::move(debug)) {
        }

    private:
        debug_config_type _debug;

        friend class articuno::access;
    };

    using DefaultPluginConfig = PluginConfig<DebugConfig>;
}
