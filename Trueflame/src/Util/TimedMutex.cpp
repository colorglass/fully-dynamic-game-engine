#include <FDGE/Util/TimedMutex.h>

using namespace FDGE::Util;

TimedMutex::TimedMutex() noexcept {
}

TimedMutex::~TimedMutex() noexcept {
}

void TimedMutex::lock() const noexcept {
    return _lock.lock();
}

bool TimedMutex::try_lock() const noexcept {
    return _lock.try_lock();
}

bool TimedMutex::try_lock_for_impl(const std::chrono::milliseconds& timeout_duration) {
    return _lock.try_lock_for(timeout_duration);
}

bool TimedMutex::try_lock_until_impl(
        const std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>& timeout_time) {
    return _lock.try_lock_until(timeout_time);
}

void TimedMutex::unlock() const noexcept {
    return _lock.unlock();
}
