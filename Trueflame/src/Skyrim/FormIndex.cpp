#include <FDGE/Skyrim/FormIndex.h>

using namespace FDGE::Skyrim;
using namespace RE;

thread_local FormIndex::State FormIndex::_currentState;

FormIndex::~FormIndex() noexcept {
    if (--_currentState.Depth == 0) {
        if (_currentState.FormIDLock) {
            TESForm::GetAllForms().second.get().UnlockForRead();
        }
        if (_currentState.EditorIDLock) {
            TESForm::GetAllFormsByEditorID().second.get().UnlockForRead();
        }
    }
}

gluino::optional_ptr<TESForm> FormIndex::operator[](FormID formID) noexcept {
    auto allForms = TESForm::GetAllForms();
    if (!_currentState.FormIDLock) {
        _currentState.FormIDLock = true;
        allForms.second.get().LockForRead();
    }
    auto result = allForms.first->find(formID);
    return result != allForms.first->end() ? result->second : nullptr;
}

gluino::optional_ptr<TESForm> FormIndex::operator[](BSFixedString editorID) noexcept {
    auto allForms = TESForm::GetAllFormsByEditorID();
    if (!_currentState.EditorIDLock) {
        _currentState.EditorIDLock = true;
        allForms.second.get().LockForRead();
    }
    auto result = allForms.first->find(editorID);
    return result != allForms.first->end() ? result->second : nullptr;
}

gluino::optional_ptr<TESForm> FormIndex::Lookup(const PortableID& portableID, FormType formType) noexcept {
    if (portableID.IsNull()) {
        return {};
    }
    if (portableID.UsesEditorIDs()) {
        for (auto& editorID: portableID.GetEditorIDs()) {
            auto result = (*this)[BSFixedString(editorID)];
            if (result && (formType == FormType::None || result->GetFormType() == formType)) {
                return result;
            }
        }
        return {};
    }
    if (portableID.IsSaveGameID()) {
        for (auto& id: portableID.GetFormIDs()) {
            auto result = (*this)[id];
            if (result && (formType == FormType::None || result->GetFormType() == formType)) {
                return result;
            }
        }
        return {};
    }
    auto files = portableID.GetFileNames();
    for (auto& id: portableID.GetFormIDs()) {
        auto result = (*this)[id];
        if (result && (formType == FormType::None || result->GetFormType() == formType)) {
            gluino::str_equal_to<false> equals;
            for (auto& file : files) {
                if (!result->sourceFiles.array) {
                    // Special handling for executable built-in forms. They can be referenced as belonging to Skyrim.esm
                    // or the SkyrimSE.exe or Skyrim.exe executable files.
                    if (equals(file, std::string_view("Skyrim.esm")) ||
                        equals(file, std::string_view("SkyrimSE.exe")) ||
                        equals(file, std::string_view("Skyrim.exe"))) {
                        return result;
                    }
                    continue;
                }
                if (equals(file, result->GetFile(0)->fileName)) {
                    return result;
                }
            }
        }
    }
    return {};
}
