#pragma once

#include <QtQuick/QQuickRenderTarget>
#include <QtQuick/QQuickWindow>

#include "OverlayLayer.h"

namespace FDGE::UI {
    class OverlayWindow;

    class LayerHandle {
    public:
        LayerHandle() = default;

        LayerHandle(const LayerHandle&) = delete;

        inline LayerHandle(LayerHandle&& other) noexcept {
            *this = std::move(other);
        }

        inline ~LayerHandle();

        LayerHandle& operator=(const LayerHandle&) = delete;

        inline LayerHandle& operator=(LayerHandle&& other) noexcept {
            _window = other._window;
            _item = other._item;
            other._item = nullptr;
            return *this;
        }

    private:
        inline LayerHandle(OverlayWindow* window, OverlayLayer* item) noexcept : _window(window), _item(item) {}

        mutable OverlayWindow* _window{nullptr};
        mutable OverlayLayer* _item{nullptr};

        friend class OverlayWindow;
    };

    class OverlayWindow {
    public:
        explicit OverlayWindow(const QQuickRenderTarget& renderTarget);

        LayerHandle AddLayer(OverlayLayer* layer);

        bool RemoveLayer(OverlayLayer* layer);

        inline bool RemoveLayer(const LayerHandle& handle) {
            if (handle._item) {
                auto result = RemoveLayer(handle._item);
                handle._item = nullptr;
                return result;
            }
            return false;
        }

        inline bool RemoveLayer(const LayerHandle&& handle) {
            return RemoveLayer(handle);
        }

    private:
        QQuickWindow* _window;
    };

    LayerHandle::~LayerHandle() {
        if (_window && _item) {
            _window->RemoveLayer(*this);
        }
    }
}
