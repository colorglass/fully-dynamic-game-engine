#pragma once

#include <RE/Skyrim.h>

namespace FDGE::Console {
    static inline void Print(std::string_view message) {
        RE::ConsoleLog::GetSingleton()->Print(message.data());
    }

    template <class... FmtArgs>
    static inline void Print(fmt::format_string<FmtArgs...> fmt, FmtArgs... args) {
        auto msg = fmt::format(fmt, std::forward<FmtArgs>(args)...);
        RE::ConsoleLog::GetSingleton()->Print(msg.c_str());
    }

    template <class... FmtArgs>
    static inline void Print(fmt::basic_runtime<char> fmt, FmtArgs... args) {
        auto msg = fmt::format(fmt, std::forward<FmtArgs>(args)...);
        RE::ConsoleLog::GetSingleton()->Print(msg.c_str());
    }
}
