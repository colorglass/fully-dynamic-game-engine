#pragma once

#include <format>
#include <stdexcept>
#include <string_view>

#include <RE/Skyrim.h>

#include "Conversion.h"
#include "Parser.h"

namespace FDGE::Console {
    class __declspec(dllexport) Execution {
    public:
        inline Execution() noexcept = default;

        inline explicit Execution(Parser& parser) noexcept : _parser(parser) {
        }

        void NoExplicitTarget() const;

        void NoTarget() const;

        template <class T>
        [[nodiscard]] inline T Target(bool allowImplicit = true) const {
            auto result = OptionalTarget<T>(allowImplicit);
            if (result.has_value()) {
                return result.value();
            }
            throw std::invalid_argument("Command can only be used on a target object.");
        }

        template <class T>
        [[nodiscard]] std::optional<T> OptionalTarget(bool allowImplicit = true) const {
            if (_parser.IsExplicitlyNonTargeting()) {
                return {};
            }
            auto result = _parser.GetExplicitTarget();
            if (result.empty()) {
                if constexpr (std::is_base_of_v<RE::TESObjectREFR,
                        std::remove_cv_t<std::remove_pointer_t<std::remove_cv_t<T>>>>) {
                    if (allowImplicit) {
                        auto consoleID = RE::Console::GetSelectedRef();
                        if (consoleID) {
                            auto* castedID = consoleID.get()->As<std::remove_pointer_t<T>>();
                            if (castedID) {
                                return castedID;
                            }
                        }
                    }
                }
                return {};
            }
            try {
                Conversion::ConversionData data{result, "self"};
                return Convert<T>(data);
            } catch (const std::invalid_argument&) {
                throw std::invalid_argument("Target of command is not a valid type.");
            }
        }

        template <class T>
        [[nodiscard]] inline T OptionalTarget(const T&& defaultValue, bool allowImplicit = true) const {
            auto result = OptionalTarget<T>(allowImplicit);
            if (result.has_value()) {
                return result.value();
            }
            return defaultValue;
        }

        template <class T>
        [[nodiscard]] T Parameter(std::string_view name) const {
            auto result = OptionalParameter<T>(name);
            if (result.has_value()) {
                return result.value();
            }
            throw std::invalid_argument(fmt::format("Required parameter '--{}' was not given.", name));
        }

        template <class T>
        [[nodiscard]] inline std::optional<T> OptionalParameter(std::string_view name) const {
            auto result = _parser.GetParameter(name);
            if (result.empty()) {
                return {};
            }
            Conversion::ConversionData data{result, name};
            return Convert<T>(data);
        }

        template <class T>
        [[nodiscard]] inline T OptionalParameter(std::string_view name, const T&& defaultValue) const {
            auto result = OptionalParameter<T>(name);
            if (result.has_value()) {
                return result.value();
            }
            return defaultValue;
        }

        [[nodiscard]] inline bool Flag(std::string_view name, std::string_view shortName = "") const {
            return _parser.HasFlag(name, shortName);
        }

        template <class T>
        [[nodiscard]] inline T NextPositional(std::string_view name) const {
            auto result = NextOptionalPositional<T>();
            if (result.has_value()) {
                return result.value();
            }
            throw std::invalid_argument(fmt::format("Required positional argument '{}' was not given.", name));
        }

        template <class T>
        [[nodiscard]] inline T NextPositional(std::size_t index) const {
            auto result = NextOptionalPositional<T>();
            if (result.has_value()) {
                return result.value();
            }
            throw std::invalid_argument(fmt::format("Required positional argument at index {} was not given.", index));
        }

        template <class T>
        [[nodiscard]] inline std::optional<T> NextOptionalPositional() const {
            std::string_view arg;
            if (!_parser.NextPositionalArgument(arg)) {
                return {};
            }
            Conversion::ConversionData data{arg};
            return Convert<T>(data);
        }

        template <class T>
        [[nodiscard]] inline std::optional<T> NextOptionalPositional(const T&& defaultValue) const {
            auto result = NextOptionalPositional<T>();
            if (result.has_value()) {
                return result.value();
            }
            return defaultValue;
        }

        template <class T>
        void SetResult([[maybe_unused]] const T&& result) const {
            // TODO:
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_parser, "parser");
        }

        Parser _parser;

        friend class articuno::access;
    };
}
