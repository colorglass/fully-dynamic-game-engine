#pragma once

#include <FDGE/Skyrim/Concepts.h>
#include <gluino/flags.h>
#include <gluino/ptr.h>
#include <gluino/virtual_cast.h>
#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    class FormPropertyCollection;

    gluino_flags(FormPropertyFlags, uint32_t,
                 (Ephemeral, 1 << 0),
                 (ClearOnReset, 1 << 1),
                 (ClearOnDeath, 1 << 2),
                 (ClearOnUnload, 1 << 3),
                 (ClearOnDisabled, 1 << 4),
                 (ClearOnZoneLevelReset, 1 << 5));

    class FormProperty {
    public:
        using value_type = std::variant<nullptr_t, RE::TESForm*, RE::ActiveEffect*,
                RE::BGSBaseAlias*, RE::BSTSmartPointer<RE::BSScript::Object>, std::string, uint64_t, int64_t, double_t>;

        template <gluino::arithmetic T>
        [[nodiscard]] inline T GetValue() const noexcept {
            if (std::holds_alternative<nullptr_t>(_value) || std::holds_alternative<RE::TESForm*>(_value)) {
                return {};
            }
            return std::visit([](const auto&& value) {
                return static_cast<T>(value);
            }, _value);
        }

        template <Form T>
        [[nodiscard]] inline gluino::optional_ptr<T> GetValue() const noexcept {
            if (std::holds_alternative<nullptr_t>(_value)) {
                return {};
            }
            return gluino::virtual_cast<T*>(std::get<RE::TESForm*>(_value));
        }

        [[nodiscard]] inline FormPropertyFlags& Flags() noexcept {
            return _flags;
        }

        [[nodiscard]] inline const FormPropertyFlags& Flags() const noexcept {
            return _flags;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_flags, "flags");
        }

        template <class T>
        inline FormProperty(T&& value, FormPropertyFlags flags) noexcept : _flags(flags) {
            if constexpr (std::is_pointer_v<T>) {
                if (value) {
                    _value = value;
                } else {
                    _value = nullptr;
                }
            } else {
                _value = value;
            }
        }

        value_type _value;
        FormPropertyFlags _flags;

        friend class articuno::access;

        friend class FormPropertyCollection;
    };
}
