#pragma once

#include <vector>

#include "RuleModule.h"
#include "SchemaVersioned.h"

namespace FDGE::Dymod {
    class Shard : public SchemaVersioned, public Node {
    public:
        [[nodiscard]] inline std::vector<std::shared_ptr<RuleModule>>& GetModules() noexcept {
            return _modules;
        }

        [[nodiscard]] inline const std::vector<std::shared_ptr<RuleModule>>& GetModules() const noexcept {
            return _modules;
        }

    private:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_modules, "modules", flags);
            articuno_super(Node, ar, flags);
            articuno_super(SchemaVersioned, ar, flags);
        }

        std::vector<std::shared_ptr<RuleModule>> _modules;

        friend class articuno::access;
    };
}
