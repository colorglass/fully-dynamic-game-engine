#pragma once

#include <memory>

namespace FDGE::Dymod {
    class Mod;

    class DymodEvent {
    public:
        [[nodiscard]] inline const std::shared_ptr<Mod>& GetMod() const noexcept {
            return _mod;
        }

    protected:
        inline explicit DymodEvent(std::shared_ptr<Mod> mod) noexcept
                : _mod(std::move(mod)) {
        }

    private:
        std::shared_ptr<Mod> _mod;
    };

    class DymodUnloadingEvent : public DymodEvent {
    public:
        inline explicit DymodUnloadingEvent(std::shared_ptr<Mod> mod) noexcept
                : DymodEvent(std::move(mod)) {
        }
    };

    class DymodUnloadedEvent : public DymodEvent {
    public:
        inline explicit DymodUnloadedEvent(std::shared_ptr<Mod> mod) noexcept
                : DymodEvent(std::move(mod)) {
        }
    };

    class DymodLoadingEvent : public DymodEvent {
    public:
        inline explicit DymodLoadingEvent(std::shared_ptr<Mod> mod) noexcept
                : DymodEvent(std::move(mod)) {
        }
    };

    class DymodLoadedEvent : public DymodEvent {
    public:
        inline explicit DymodLoadedEvent(std::shared_ptr<Mod> mod) noexcept
                : DymodEvent(std::move(mod)) {
        }
    };

    class DymodReloadingEvent : public DymodEvent {
    public:
        inline explicit DymodReloadingEvent(std::shared_ptr<Mod> mod) noexcept
                : DymodEvent(std::move(mod)) {
        }
    };

    class DymodReloadedEvent : public DymodEvent {
    public:
        inline explicit DymodReloadedEvent(std::shared_ptr<Mod> mod) noexcept
                : DymodEvent(std::move(mod)) {
        }
    };
}
