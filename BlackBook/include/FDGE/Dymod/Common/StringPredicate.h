#pragma once

#include <FDGE/Util/Maps.h>
#include <srell.hpp>
#include <tsl/htrie_set.h>

#include "../Predicate.h"

namespace FDGE::Dymod::Common {
    class StringPredicate : public Predicate {
    public:
        bool AddPrefix(std::string_view prefix) noexcept;

        bool RemovePrefix(std::string_view prefix) noexcept;

        bool AddSuffix(std::string_view suffix) noexcept;

        bool RemoveSuffix(std::string_view suffix) noexcept;

        [[nodiscard]] inline Util::istr_flat_hash_set<std::string>& GetValues() noexcept {
            return _values;
        }

        [[nodiscard]] inline const Util::istr_flat_hash_set<std::string>& GetValues() const noexcept {
            return _values;
        }

        [[nodiscard]] inline const tsl::htrie_set<char>& GetPrefixes() const noexcept {
            return _prefixes;
        }

        [[nodiscard]] inline const tsl::htrie_set<char>& GetSuffixes() const noexcept {
            return _suffixes;
        }

        [[nodiscard]] inline std::vector<srell::regex>& GetPatterns() noexcept {
            return _patterns;
        }

        [[nodiscard]] inline const std::vector<srell::regex>& GetPatterns() const noexcept {
            return _patterns;
        }

    protected:
        [[nodiscard]] virtual std::optional<std::string> GetValue(const gluino::polymorphic_any& target) const noexcept = 0;

        [[nodiscard]] PredicateResult TestImpl(const gluino::polymorphic_any& target) const noexcept override;

    private:
        Util::istr_flat_hash_set<std::string> _values;
        tsl::htrie_set<char> _prefixes;
        tsl::htrie_set<char> _suffixes;
        std::vector<srell::regex> _patterns;
    };
}
