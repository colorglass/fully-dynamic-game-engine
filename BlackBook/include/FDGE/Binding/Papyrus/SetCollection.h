#pragma once

#include "Collection.h"

namespace FDGE::Binding::Papyrus {
    class SetCollection : public Collection {
    ScriptType(SetCollection)
    public:
        [[nodiscard]] virtual Any* GetValue(Any* key) noexcept = 0;

        [[nodiscard]] virtual Any* PutValue(Any* value) noexcept = 0;

        [[nodiscard]] virtual bool TryPutValue(Any* value) noexcept = 0;

        [[nodiscard]] virtual Any* Delete(Any* value) noexcept = 0;

        [[nodiscard]] virtual bool Contains(Any* value) const noexcept = 0;

        [[nodiscard]] virtual ValueIterator* GetValueIteratorFrom(Any* value) noexcept = 0;

    protected:
        inline SetCollection() noexcept = default;

    private:
        articuno_serde() {}

        friend class articuno::access;
    };
}
