#pragma once

#include <articuno/types/auto.h>
#include <articuno/types/parallel_hashmap/phmap.h>
#include <FDGE/Logger.h>
#include <FDGE/Util/EventSource.h>
#include <FDGE/Util/Maps.h>
#include <FDGE/Util/RecursiveMutex.h>
#include <FDGE/Util/SafeAlloc.h>

#include "../../Hook/Serialization.h"
#include "../../System.h"

#undef GetObject

namespace FDGE::Binding::Papyrus {
    class ScriptObject;

    class ScriptObjectHandleEvent {
    public:
        [[nodiscard]] inline RE::VMHandle GetHandle() const noexcept {
            return _handle;
        }

    protected:
        inline explicit ScriptObjectHandleEvent(RE::VMHandle handle = 0) noexcept
                : _handle(handle) {
        }

    private:
        RE::VMHandle _handle{0};
    };

    class ScriptObjectEvent : public ScriptObjectHandleEvent {
    public:
        [[nodiscard]] inline ScriptObject* GetObject() const noexcept {
            return _object;
        }

    protected:
        inline explicit ScriptObjectEvent(ScriptObject* object = nullptr, RE::VMHandle handle = 0) noexcept
                : _object(object), ScriptObjectHandleEvent(handle) {
        }

    private:
        ScriptObject* _object{nullptr};
    };

    class ScriptObjectCreatedEvent : public ScriptObjectEvent {
    public:
        inline explicit ScriptObjectCreatedEvent(ScriptObject* object = nullptr, RE::VMHandle handle = 0) noexcept
            : ScriptObjectEvent(object, handle) {
        }
    };

    class ScriptObjectBoundEvent : public ScriptObjectEvent {
    public:
        inline explicit ScriptObjectBoundEvent(const RE::BSTSmartPointer<RE::BSScript::Object>& papyrusObject,
                                               ScriptObject* object = nullptr, RE::VMHandle handle = 0) noexcept
            : ScriptObjectEvent(object, handle), _papyrusObject(papyrusObject) {
        }

        [[nodiscard]] inline const RE::BSTSmartPointer<RE::BSScript::Object>& GetPapyrusObject() const noexcept {
            return _papyrusObject;
        }

    private:
        const RE::BSTSmartPointer<RE::BSScript::Object>& _papyrusObject;
    };

    class DestroyingScriptObjectEvent : public ScriptObjectEvent {
    public:
        inline explicit DestroyingScriptObjectEvent(ScriptObject* object = nullptr, RE::VMHandle handle = 0) noexcept
            : ScriptObjectEvent(object, handle) {
        }
    };

    class ScriptObjectDestroyedEvent : public ScriptObjectHandleEvent {
    public:
        inline explicit ScriptObjectDestroyedEvent(RE::VMHandle handle = 0) noexcept
            : ScriptObjectHandleEvent(handle) {
        }
    };

#pragma warning(push)
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)
    class __declspec(dllexport) ScriptObjectStore : public Util::EventSource<ScriptObjectCreatedEvent>,
                                                    public Util::EventSource<ScriptObjectBoundEvent>,
                                                    public Util::EventSource<DestroyingScriptObjectEvent>,
                                                    public Util::EventSource<ScriptObjectDestroyedEvent> {
    public:
        using Util::EventSource<ScriptObjectCreatedEvent>::Listen;
        using Util::EventSource<ScriptObjectCreatedEvent>::ListenForever;
        using Util::EventSource<ScriptObjectBoundEvent>::Listen;
        using Util::EventSource<ScriptObjectBoundEvent>::ListenForever;
        using Util::EventSource<DestroyingScriptObjectEvent>::Listen;
        using Util::EventSource<DestroyingScriptObjectEvent>::ListenForever;
        using Util::EventSource<ScriptObjectDestroyedEvent>::Listen;
        using Util::EventSource<ScriptObjectDestroyedEvent>::ListenForever;

        gluino::optional_ptr<ScriptObject> FindObject(RE::VMHandle id) const noexcept;

        RE::VMHandle GetID(ScriptObject* object) const noexcept;

        RE::VMHandle Add(ScriptObject* object);

        bool Remove(RE::VMHandle id) noexcept;

        static ScriptObjectStore& GetSingleton() noexcept;

    protected:
        using Util::EventSource<ScriptObjectCreatedEvent>::Emit;
        using Util::EventSource<ScriptObjectBoundEvent>::Emit;
        using Util::EventSource<DestroyingScriptObjectEvent>::Emit;
        using Util::EventSource<ScriptObjectDestroyedEvent>::Emit;

    private:
        inline ScriptObjectStore() {
            Hook::RegisterSaveHandler("ScriptObjectStore", [this](std::ostream& out) { OnGameSaved(out); });
            Hook::RegisterLoadHandler("ScriptObjectStore", [this](std::istream& in) { OnGameLoaded(in); });
            _registration = System().Listen([this](const NewGameEvent&) { OnNewGame(); });
        }

        void OnNewGame();

        void OnGameSaved(std::ostream& out);

        void OnGameLoaded(std::istream& in);

        articuno_serialize(ar) {
            ar <=> articuno::kv(_objectsToIDs, "objects", articuno::value_flags::allow_indirection);
            ar <=> articuno::kv(_nextID, "nextID");
        }

        articuno_deserialize(ar) {
            ar <=> articuno::kv(_objectsToIDs, "objects", articuno::value_flags::allow_indirection);
            ar <=> articuno::kv(_nextID, "nextID");

            _idsToObjects.reserve(_objectsToIDs.size());
            for (auto& entry : _objectsToIDs) {
                _idsToObjects.try_emplace(entry.second, entry.first);
            }
        }

        mutable Util::RecursiveMutex _lock;
        phmap::flat_hash_map<RE::VMHandle, ScriptObject*> _idsToObjects;
        phmap::flat_hash_map<ScriptObject*, RE::VMHandle> _objectsToIDs;
        uint64_t _nextID{1};
        Util::EventRegistration _registration;

        friend class articuno::access;
        friend class ScriptObject;
        friend __declspec(dllexport) void PackScriptObjectHandle(RE::BSScript::Variable*, void*, RE::VMTypeID);
    };

    /**
     * The base class of all custom script objects.
     *
     * <p>
     * All custom script objects must inherit from this class. On the Papyrus side this class maps to a
     * <code>ScriptObject</code> script. The Papyrus hierarchy should match the C++ hierarchy. To implement a child
     * class, you must also override the <code>GetTypeName</code> function and define a static <code>TypeName</code>
     * string-view. This is simplest done with the <code>ScriptType</code> macro.
     * </p>
     */
    class __declspec(dllexport) ScriptObject {
    public:
        static constexpr std::string_view TypeName = "ScriptObject";

        [[nodiscard]] void* operator new(std::size_t size);

        [[nodiscard]] void* operator new(std::size_t size, void* object);

        void operator delete(void* object) noexcept;

        void operator delete(void* object, void* place) noexcept;

        inline ScriptObject() noexcept = default;

        virtual ~ScriptObject() noexcept = default;

        [[nodiscard]] RE::BSTSmartPointer<RE::BSScript::Object> GetPapyrusObject() const noexcept;

        [[nodiscard]] virtual std::string_view GetTypeName() const noexcept;

        [[nodiscard]] virtual RE::BSFixedString ToString() const noexcept;

        [[nodiscard]] virtual std::size_t GetHashCode() const noexcept;

        [[nodiscard]] virtual bool Equals(const ScriptObject* other) const noexcept;

        RE::BSTSmartPointer<RE::BSScript::Object> Bind(std::string_view classNameOverride = "") noexcept;

        template <class T>
        static void Register(std::string_view typeName, const articuno::type_registration& source_reg,
                             const articuno::type_registration& sink_reg) {
            Register(typeName, source_reg, sink_reg, []() -> ScriptObject* {
                if constexpr (std::default_initializable<T>) {
                    return new T();
                } else {
                    return nullptr;
                }
            });
        }

        static void FlushDecRefs();

        inline static bool IsScriptObject(RE::VMHandle handle) {
            return handle >= 0x0004000000000000;
        }

        [[nodiscard]] static gluino::optional_ptr<ScriptObject> FromPapyrusObject(
                const RE::BSTSmartPointer<RE::BSScript::Object>& object) noexcept;

    protected:
        virtual void OnGameLoadComplete();

    private:
        articuno_serde() {
        }

        static void Register(std::string_view typeName, const articuno::type_registration& source_reg,
                             const articuno::type_registration& sink_reg, std::function<ScriptObject*()> ctor);

        friend class articuno::access;
        friend class ScriptObjectStore;
    };
#pragma warning(pop)

    template <class T>
    struct is_script_object : public std::is_base_of<ScriptObject, T> {
    };

    template <class T>
    static constexpr bool is_script_object_v = is_script_object<T>::value;

    void __declspec(dllexport) PackScriptObjectHandle(
            RE::BSScript::Variable* destination, void* source, RE::VMTypeID typeID);

    /**
     * A type-safe handle for a script object which guarantees it is not garbage collected.
     *
     * <p>
     * The script object handle holds a reference to the underlying Papyrus object that backs the script object, which
     * prevents the Papyrus reference counting system from freeing the memory (and therefore releasing the script
     * object as well). This handle is intended for use when storing a reference to a script object from another script
     * object. It is serializable, allowing the object to be recovered during deserialization.
     * </p>
     *
     * <p>
     * The underlying script object type is a template parameter, which allows it to be strongly typed. When retrieving
     * the underlying object it will be returned if found and the correct type. Getting back the original script object
     * requires a hash table lookup so the results should be cached when possible.
     * </p>
     *
     * @tparam T The type of script object to which the handle refers.
     */
    template <class T>
    class ScriptObjectHandle {
    public:
        using value_type = T;
        using pointer_type = value_type*;

        inline ScriptObjectHandle() noexcept = default;

        inline ScriptObjectHandle(nullptr_t) noexcept {
        };

        inline ScriptObjectHandle(ScriptObject* object) {
            if (!object) {
                Logger::Warn("Attempt to make a ScriptObjectHandle for a null script.");
                return;
            }
            _papyrusObject = object->Bind();
            if (!_papyrusObject) {
                Logger::Warn("Attempt to make a ScriptObjectHandle for a detached script object.");
            }
        }

        inline ScriptObjectHandle(RE::BSTSmartPointer<RE::BSScript::Object> object) noexcept
                : _papyrusObject(std::move(object)) {
        }

        [[nodiscard]] inline T* GetObject() noexcept {
            if (!_papyrusObject.get() || !_papyrusObject->GetHandle()) {
                return nullptr;
            }
            auto* obj =ScriptObjectStore::GetSingleton().FindObject(_papyrusObject->GetHandle()).get_raw();
            if (!obj) {
                return nullptr;
            }
            return dynamic_cast<T*>(obj);
        }

        [[nodiscard]] inline const T* GetObject() const noexcept {
            if (!_papyrusObject->GetHandle()) {
                return nullptr;
            }
            return dynamic_cast<const T*>(
                    ScriptObjectStore::GetSingleton().FindObject(_papyrusObject->GetHandle()).get_raw());
        }

        [[nodiscard]] inline operator bool() const noexcept {
            return _papyrusObject.get() != nullptr;
        }

        [[nodiscard]] inline operator T*() noexcept {
            return GetObject();
        }

        [[nodiscard]] inline operator const T*() const noexcept {
            return GetObject();
        }

        [[nodiscard]] inline operator RE::BSTSmartPointer<RE::BSScript::Object>() const noexcept {
            return _papyrusObject;
        }

        [[nodiscard]] inline operator RE::BSTSmartPointer<RE::BSScript::Object>&() noexcept {
            return _papyrusObject;
        }

        [[nodiscard]] inline operator const RE::BSTSmartPointer<RE::BSScript::Object>&() const noexcept {
            return _papyrusObject;
        }

        [[nodiscard]] inline auto operator*() const noexcept {
            return *_papyrusObject;
        }

        [[nodiscard]] inline auto operator->() const noexcept {
            return _papyrusObject.operator->();
        }

        [[nodiscard]] bool operator==(const ScriptObjectHandle<T>& other) const noexcept {
            auto* objA = GetObject();
            auto* objB = other.GetObject();
            if (!objA) {
                return objB == nullptr;
            }
            return objA->Equals(objB);
        }

    private:
        articuno_serialize(ar) {
            if (_papyrusObject) {
                ar <=> articuno::self(_papyrusObject->handle);
            } else {
                RE::VMHandle zero = 0;
                ar <=> articuno::self(zero);
            }
        }

        articuno_deserialize(ar) {
            _papyrusObject.reset();
            RE::VMHandle handle;
            ar <=> articuno::self(handle);
            if (!handle) {
                return;
            }
            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            RE::BSFixedString className;
            {
                RE::BSSpinLockGuard lock(vm->attachedScriptsLock);
                auto result = vm->attachedScripts.find(handle);
                if (result == vm->attachedScripts.end()) {
                    return;
                }
                className = (*result->second.begin())->GetTypeInfo()->GetName();
            }
            vm->FindBoundObject(handle, className.c_str(), _papyrusObject);
        }

        RE::BSTSmartPointer<RE::BSScript::Object> _papyrusObject;

        friend class articuno::access;
    };

    template <class T>
    class WeakScriptObjectHandle {
    public:
        using value_type = T;
        using pointer_type = value_type*;

        inline WeakScriptObjectHandle() noexcept = default;

        explicit WeakScriptObjectHandle(const ScriptObjectHandle<T>& handle) noexcept {
            if (handle) {
                _handle = handle->GetHandle();
            }
        }

        [[nodiscard]] ScriptObjectHandle<T> Lock() const noexcept {
            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            if (!vm) {
                return {};
            }

            auto obj = ScriptObjectStore::GetSingleton().FindObject(_handle);
            if (!obj) {
                return {};
            }
            RE::BSTSmartPointer<RE::BSScript::Object> objectPtr;
            vm->FindBoundObject(_handle, obj->GetTypeName().data(), objectPtr);
            return std::move(objectPtr);
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::self(_handle);
        }

        RE::VMHandle _handle{0};

        friend class articuno::access;
    };
};

namespace RE::BSScript {
    template <class T>
    requires (FDGE::Binding::Papyrus::is_script_object_v<T>)
    struct is_valid_base<T*> : public std::true_type {
    };

    template <class T>
    requires (FDGE::Binding::Papyrus::is_script_object_v<T>)
    struct is_parameter_convertible<T*> : public std::true_type {
    };

    template <class T>
    requires (FDGE::Binding::Papyrus::is_script_object_v<T>)
    struct is_return_convertible<T*> : public std::true_type {
    };

    template <class T>
    requires (std::is_pointer_v<T> && FDGE::Binding::Papyrus::is_script_object_v<unwrapped_type_t<T>>)
    struct GetRawType<T> {
        [[nodiscard]] inline TypeInfo::RawType operator()() const noexcept {
            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            if (!vm) {
                FDGE::Logger::Warn("Attempt to find raw type of script object before virtual machine initialization.");
                return TypeInfo::RawType::kNone;
            }
            auto typeName = unwrapped_type_t<T>::TypeName;
            auto result = vm->objectTypeToTypeID.find(typeName);
            if (result == vm->objectTypeToTypeID.end()) {
                FDGE::Logger::Warn("Attempt to find raw type of unregistered script object type {}.", typeName);
                return TypeInfo::RawType::kNone;
            }
            return GetRawTypeFromVMType(result->second);
        }
    };

    template <class T>
    requires ((is_array_v<T> || is_reference_wrapper_v<T>) &&
              FDGE::Binding::Papyrus::is_script_object_v<unwrapped_type_t<typename T::value_type>>)
    struct GetRawType<T> {
        [[nodiscard]] inline TypeInfo::RawType operator()() const noexcept {
            auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
            if (!vm) {
                FDGE::Logger::Warn("Attempt to find raw type of script object before virtual machine initialization.");
                return TypeInfo::RawType::kNone;
            }
            auto typeName = unwrapped_type_t<typename T::value_type>::TypeName;
            auto result = vm->objectTypeToTypeID.find(typeName);
            if (result == vm->objectTypeToTypeID.end()) {
                FDGE::Logger::Warn("Attempt to find raw type of unregistered script object array of type {}.",
                                   typeName);
                return TypeInfo::RawType::kNone;
            }
            return *(stl::enumeration{ GetRawTypeFromVMType(result->second) } +
                     TypeInfo::RawType::kObject);
        }
    };

    template <class T, class U = ::std::decay_t<T>,
            ::std::enable_if_t<
                    ::std::conjunction_v<::std::is_pointer<std::remove_cvref_t<T>>,
                            ::FDGE::Binding::Papyrus::is_script_object<std::remove_pointer_t<std::remove_cvref_t<T>>>>,
                    int> = 0>
    inline void PackValue(::RE::BSScript::Variable* destination, T&& source) {
        auto* vm = Internal::VirtualMachine::GetSingleton();
        if (!vm || !source) {
            return;
        }
        VMTypeID typeID;
        auto result = vm->objectTypeToTypeID.find(std::remove_pointer_t<std::remove_cvref_t<T>>::TypeName);
        if (result == vm->objectTypeToTypeID.end()) {
            FDGE::Logger::Error("Cannot pack unregistered ScriptObject type {}.", source->GetTypeName());
            return;
        }
        typeID = result->second;
        FDGE::Binding::Papyrus::PackScriptObjectHandle(destination, source, typeID);
    }

    template <
            class T, class U,
            std::enable_if_t<is_array_v<U>, int> = 0>
    requires (FDGE::Binding::Papyrus::is_script_object_v<unwrapped_type_t<typename U::value_type>>)
    void PackValue(Variable* destination, T&& source) {
        if (!destination) {
            return;
        }
        destination->SetNone();

        auto vm = Internal::VirtualMachine::GetSingleton();
        if (!vm) {
            return;
        }

        BSTSmartPointer<Array> array;
        TypeInfo typeInfo(GetRawType<typename U::value_type>{}());
        if (!vm->CreateArray(typeInfo, static_cast<std::uint32_t>(source.size()), array) || !array) {
            return;
        }

        auto it = source.begin();
        auto end = source.end();
        std::uint32_t i = 0;
        while (it != end) {
            if constexpr (std::is_same_v<U, std::vector<bool>>) {
                (*array)[i++].Pack(static_cast<bool>(*it));
            } else {
                (*array)[i++].Pack(*it);
            }
            ++it;
        }

        destination->SetArray(std::move(array));
    }

    template <class T,
            ::std::enable_if_t<
                    ::std::conjunction_v<::std::is_pointer<T>,
                            ::FDGE::Binding::Papyrus::is_script_object<std::remove_pointer_t<T>>>,
                    int> = 0>
    [[nodiscard]] inline T UnpackValue(const ::RE::BSScript::Variable* source) {
        auto* vm = Internal::VirtualMachine::GetSingleton();
        if (!vm || !source || source->IsNoneObject()) {
            FDGE::Logger::Error("Unable to unpack script object, invalid input.");
            return nullptr;
        }
        auto object = source->GetObject();
        if (!object) {
            FDGE::Logger::Warn("No object associated with variable being unpacked, but variable was not marked None.");
            return nullptr;
        }
        VMTypeID typeID{0};
        for (auto* typeInfo = object->GetTypeInfo(); typeInfo; typeInfo = typeInfo->GetParent()) {
            auto result = vm->objectTypeToTypeID.find(typeInfo->GetName());
            if (result != vm->objectTypeToTypeID.end()) {
                typeID = result->second;
                break;
            }
        }
        if (!typeID) {
            FDGE::Logger::Error("Unable to unpack script object, object type '{}' is not registered.",
                                            object->GetTypeInfo()->GetName());
            return nullptr;
        }
        return static_cast<T>(UnpackHandle(source, typeID));
    }

    template <class T,
            std::enable_if_t<is_array_v<T>, int> = 0>
    requires (FDGE::Binding::Papyrus::is_script_object_v<typename std::decay_t<T>::value_type>)
    [[nodiscard]] T UnpackValue(const Variable* source) {
        if (!source) {
            return typename std::decay_t<T>();
        }

        std::remove_const_t<T> container;
        if (source->IsNoneObject()) {
            return container;
        }

        auto array = source->GetArray();
        if (!array) {
            assert(false);
            return container;
        }

        for (auto& elem : *array) {
            container.push_back(elem.Unpack<typename T::value_type>());
        }

        return container;
    }
}

/**
 * Defines the Papyrus script name for a script object class.
 *
 * <p>
 * This is a convenience macro which defines the necessary constants and overrides to associate a C++ class with a
 * Papyrus script properly. It should be placed within the body of your class definition, by convention in the first
 * line of the class body. The argument is taken in the form of a C++ identifier, not a string.
 * </p>
 */
#define ScriptType(name) public: \
    static constexpr std::string_view TypeName = #name; \
                                 \
    [[nodiscard]] inline std::string_view GetTypeName() const noexcept override { \
        return TypeName;         \
    };                           \
                                 \
    private:

/**
 * Registers a new <code>ScriptObject</code> descendant class for use with Papyrus.
 *
 * <p>
 * Every script object must be registered to be used with Papyrus. Invoke this macro with the C++ class name as the
 * argument at the top-level or within a namespace to auto-register your type during initialization. This also registers
 * the types with Articuno for proper serialization handling. This registration should be done once within one of your
 * source files, not within the header file.
 * </p>
 */
#define RegisterScriptType(cppClass) namespace { \
    OnSKSELoad(20) {                             \
        ::articuno::type_registry<::articuno::ryml::yaml_source<>::archive_type>::get().register_type<cppClass>(); \
        ::articuno::type_registry<::articuno::ryml::yaml_sink<>::archive_type>::get().register_type<cppClass>();   \
        ::articuno::type_registration source_reg =                                                                 \
            ::articuno::type_registration::create<::articuno::ryml::yaml_source<>::archive_type, cppClass>();      \
        ::articuno::type_registration sink_reg = \
            ::articuno::type_registration::create<::articuno::ryml::yaml_sink<>::archive_type, cppClass>();        \
        ::FDGE::Binding::Papyrus::ScriptObject::Register<cppClass>(cppClass::TypeName, source_reg, sink_reg);      \
    }                                            \
}

namespace articuno::serde {
    template <::articuno::archive Archive, class T>
    requires (FDGE::Binding::Papyrus::is_script_object_v<T>)
    [[nodiscard]] inline ::gluino::uncvref_t<T>* allocate(Archive&, ::articuno::value_flags, ::std::size_t count = 1) {
        return FDGE::Util::SafeAlloc(sizeof(T) * count);
    }
}
