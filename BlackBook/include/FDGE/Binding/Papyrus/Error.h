#pragma once

#include "StackFrame.h"

namespace FDGE::Binding::Papyrus {
#pragma warning(push)
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)
    class __declspec(dllexport) Error : public ScriptObject {
        ScriptType(Error);

    public:
        inline Error() noexcept = default;

        inline explicit Error(std::string reason) noexcept
                : _reason(std::move(reason)) {
        }

        inline Error(std::string reason, ScriptObjectHandle<Error> cause)
                : _reason(std::move(reason)), _cause(std::move(cause)) {
        }

        inline Error(std::string reason, RE::BSScript::StackFrame* stack, bool includeNativeFrames = false)
                : _reason(std::move(reason)) {
            InitializeStack(stack, includeNativeFrames);
        }

        inline Error(std::string reason, ScriptObjectHandle<Error> cause, RE::BSScript::StackFrame* stack,
                     bool includeNativeFrames = false)
                : _reason(std::move(reason)), _cause(std::move(cause)) {
            InitializeStack(stack, includeNativeFrames);
        }

        [[nodiscard]] inline std::string_view GetReason() const noexcept {
            return _reason;
        }

        [[nodiscard]] inline Error* GetCause() noexcept {
            return _cause;
        }

        [[nodiscard]] inline std::vector<StackFrame*> GetStackTrace() noexcept {
            std::vector<StackFrame*> result;
            for (auto& frame : _stackTrace) {
                if (!frame) {
                    continue;
                }
                auto* obj = frame.GetObject();
                if (obj) {
                    result.emplace_back(obj);
                }
            }
            return result;
        }

    private:
        void InitializeStack(RE::BSScript::StackFrame* stack, bool includeNative = true);

        articuno_serde(ar) {
            ar <=> articuno::kv(_reason, "reason");
            ar <=> articuno::kv(_cause, "cause");
            ar <=> articuno::kv(_stackTrace, "stackTrace");
        }

        ScriptObjectHandle<Error> _cause;
        std::string _reason;
        std::vector<ScriptObjectHandle<StackFrame>> _stackTrace;

        friend class articuno::access;
    };
#pragma warning(pop)
}
