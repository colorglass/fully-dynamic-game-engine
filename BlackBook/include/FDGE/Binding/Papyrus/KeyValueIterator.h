#pragma once

#include "Pair.h"
#include "ValueIterator.h"

namespace FDGE::Binding::Papyrus {
    class KeyValueIterator : public ValueIterator {
        ScriptType(KeyValueIterator)

    public:
        [[nodiscard]] Any* GetValue() noexcept override;

        [[nodiscard]] Any* GetValueAndNext() noexcept override;

        [[nodiscard]] Any* NextAndGetValue() noexcept override;

        [[nodiscard]] virtual Any* GetKey() noexcept;

        [[nodiscard]] virtual Pair* GetEntry() noexcept = 0;

        [[nodiscard]] virtual Any* GetKeyAndNext() noexcept;

        [[nodiscard]] virtual Any* NextAndGetKey() noexcept;

        [[nodiscard]] virtual Pair* GetEntryAndNext() noexcept;

        [[nodiscard]] virtual Pair* NextAndGetEntry() noexcept;

    protected:
        inline KeyValueIterator() noexcept = default;

    private:
        articuno_serde() {
        }

        friend class articuno::access;
    };
}
