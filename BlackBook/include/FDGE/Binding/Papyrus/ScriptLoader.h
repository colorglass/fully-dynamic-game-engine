#pragma once

#include <RE/Skyrim.h>

namespace FDGE::Binding::Papyrus {
    class ScriptLoader {
    public:
        class LoadContext {
        public:
            inline ~LoadContext() {
                ScriptDirectory = "";
            }

            LoadContext(const LoadContext&) = delete;

            [[nodiscard]] static inline const std::filesystem::path& GetCurrentScriptDirectory() noexcept {
                return ScriptDirectory;
            }

        private:
            inline explicit LoadContext(std::filesystem::path directory) {
                ScriptDirectory = std::move(directory);
            }

            static thread_local inline std::filesystem::path ScriptDirectory;

            friend class ScriptLoader;
        };

        static void LoadScripts(std::filesystem::path directory);

    private:
    };
}
