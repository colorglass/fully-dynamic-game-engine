#pragma once

#include <articuno/types/tsl/ordered_map.h>

#include "MapCollection.h"

namespace FDGE::Binding::Papyrus {
    class HashMapIterator;

    class HashMap : public MapCollection {
        ScriptType(HashMap)

    public:
        inline HashMap() noexcept = default;

        [[nodiscard]] Any* Get(Any* key) noexcept override;

        [[nodiscard]] Any* Put(Any* key, Any* value) override;

        [[nodiscard]] bool TryPut(Any* key, Any* value) override;

        [[nodiscard]] bool Contains(Any* key) noexcept override;

        [[nodiscard]] Any* Delete(Any* key) noexcept override;

        bool Clear() noexcept override;

        void Reserve(int32_t capacity);

        [[nodiscard]] int32_t GetCapacity() const noexcept;

        [[nodiscard]] int32_t GetSize() const noexcept;

        [[nodiscard]] ValueIterator* GetValueIterator() noexcept override;

        [[nodiscard]] KeyValueIterator* GetKeyValueIterator() noexcept override;

    private:
        using AnyType = ScriptObjectHandle<Any>;
        using Allocator = std::allocator<std::pair<AnyType, AnyType>>;

        articuno_serde(ar) {
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_version, "version");
        }

        mutable std::recursive_mutex _lock;
        tsl::ordered_map<AnyType, AnyType, std::hash<AnyType>, std::equal_to<AnyType>> _value;
        uint64_t _version;

        friend class articuno::access;
        friend class HashMapIterator;
    };

    class HashMapIterator : public KeyValueIterator {
        ScriptType(HashMapIterator)

    public:
        inline HashMapIterator() noexcept = default;

        HashMapIterator(HashMap* parent, bool reverse = false);

        HashMapIterator(HashMap* parent, bool reverse, Any* key);

        [[nodiscard]] bool IsValid() noexcept override;

        [[nodiscard]] bool HasMore() noexcept override;

        bool SetValue(Any* value) noexcept override;

        bool Next() noexcept override;

        [[nodiscard]] Pair* GetEntry() noexcept override;

    protected:
        void OnGameLoadComplete() override;

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_parent, "parent");
            ar <=> articuno::kv(_key, "key");
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_version, "version");
            ar <=> articuno::kv(_reverse, "reverse");
            ar <=> articuno::kv(_beforeBegin, "beforeBegin");
        }

        ScriptObjectHandle<HashMap> _parent;
        ScriptObjectHandle<Any> _key;
        ScriptObjectHandle<Any> _value;
        uint64_t _version{0};
        bool _reverse{false};
        bool _beforeBegin{false};
        typename decltype(HashMap::_value)::iterator _iter;

        friend class articuno::access;
    };
}
