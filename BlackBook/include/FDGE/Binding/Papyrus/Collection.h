#pragma once

#include "ValueIterator.h"

namespace FDGE::Binding::Papyrus {
    class Collection : public ScriptObject {
        ScriptType(Collection);

    public:
        [[nodiscard]] virtual int32_t GetSize() const noexcept = 0;

        [[nodiscard]] virtual bool Clear() noexcept = 0;

        [[nodiscard]] virtual ValueIterator* GetValueIterator() noexcept = 0;

    protected:
        inline Collection() noexcept = default;

    private:
        articuno_serde() {
        }

        friend class articuno::access;
    };
}
