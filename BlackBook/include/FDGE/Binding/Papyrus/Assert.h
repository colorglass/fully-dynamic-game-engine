#pragma once

#include <vector>

#include <RE/Skyrim.h>

namespace FDGE::Binding::Papyrus::Assert {
    std::vector<std::string> FindAssertFailures(RE::VMHandle handle);

    __declspec(dllexport) bool TrackAssert(RE::BSTSmartPointer<RE::BSScript::Stack>&& stack, bool success,
                                           std::string_view message);
}
