#pragma once

#include <FDGE/Skyrim/Database.h>

namespace FDGE::Skyrim {
    struct ExteriorCellState {
        RE::BGSEncounterZone* EncounterZone{nullptr};
        RE::BGSLocation* Location{nullptr};
        phmap::flat_hash_map<RE::TESRegionData::Type, RE::TESRegion*> Regions;
        RE::TESWorldSpace* WorldSpace{nullptr};
    };

    extern phmap::node_hash_map<RE::FormID, sync_set<std::variant<RE::FormID, RE::TESObjectCELL*>>> locationCells;

    extern phmap::node_hash_map<RE::FormID, sync_set<std::variant<RE::FormID, RE::TESObjectCELL*>>> worldSpaceCells;

    extern phmap::node_hash_map<RE::FormID, sync_set<std::variant<RE::FormID, RE::TESObjectCELL*>>> encounterZoneCells;

    extern phmap::node_hash_map<RE::FormID, sync_set<std::variant<RE::FormID, RE::TESObjectCELL*>>> regionCells;

    extern phmap::node_hash_map<RE::FormID, ExteriorCellState> exteriorCells;
}
