#include <FDGE/Binding/Papyrus/Version.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE;

BSFixedString Version::ToString() const noexcept {
    std::ostringstream out;
    out << _major;
    out << '.';
    out << _minor;
    out << '.';
    out << _patch;
    return out.str().c_str();
}

bool Version::Equals(const ScriptObject* other) const noexcept {
    if (other == nullptr) {
        return false;
    }
    auto* otherVersion = dynamic_cast<const Version*>(other);
    if (!otherVersion) {
        return false;
    }
    return _major == otherVersion->_major && _minor == otherVersion->_minor && _patch == otherVersion->_patch;
}

namespace {
    RegisterScriptType(Version);

    PapyrusClass(Version) {
        PapyrusStaticFunction(Create, int32_t major, int32_t minor, int32_t patch) -> Version* {
            if (major < 0 || minor < 0 || patch < 0) {
                return nullptr;
            }
            return new Version(major, minor, patch);
        };

        PapyrusFunction(__GetMajor, Version* self) {
            if (!self) {
                return 0;
            }
            return self->GetMajor();
        };

        PapyrusFunction(__GetMinor, Version* self) {
            if (!self) {
                return 0;
            }
            return self->GetMinor();
        };

        PapyrusFunction(__GetPatch, Version* self) {
            if (!self) {
                return 0;
            }
            return self->GetPatch();
        };

        PapyrusFunction(NewerThan, Version* self, Version* other) {
            if (!self || !other) {
                return false;
            }
            if (self->GetMajor() > other->GetMajor()) {
                return true;
            } else if (self->GetMajor() == other->GetMajor()) {
                if (self->GetMinor() > other->GetMinor()) {
                    return true;
                } else if (self->GetMinor() == other->GetMinor()) {
                    return self->GetPatch() > other->GetPatch();
                }
            }
            return false;
        };

        PapyrusFunction(OlderThan, Version* self, Version* other) {
            if (!self || !other) {
                return false;
            }
            if (self->GetMajor() < other->GetMajor()) {
                return true;
            } else if (self->GetMajor() == other->GetMajor()) {
                if (self->GetMinor() < other->GetMinor()) {
                    return true;
                } else if (self->GetMinor() == other->GetMinor()) {
                    return self->GetPatch() < other->GetPatch();
                }
            }
            return false;
        };
    };
}
