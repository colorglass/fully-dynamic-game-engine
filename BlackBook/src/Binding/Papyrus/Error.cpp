#include <FDGE/Binding/Papyrus/Error.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <processthreadsapi.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE;
using namespace RE::BSScript;

void Error::InitializeStack(RE::BSScript::StackFrame* stack, bool includeNative) {
    auto trace = boost::stacktrace::stacktrace();

    if (includeNative) {
        std::size_t i = 0;
        bool wasNativeFunction = false;
        for (boost::stacktrace::frame frame: trace) {
            if (++i <= 4) {
                continue;
            }

            auto* address = frame.address();
            int32_t offset = 0;
            std::string moduleName;
            HMODULE module;
            if (GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS,
                                  reinterpret_cast<FDGE_WIN_CHAR_TYPE*>(reinterpret_cast<uintptr_t>(address)),
                                  &module)) {
                offset = static_cast<int32_t>(reinterpret_cast<uintptr_t>(address) -
                                              reinterpret_cast<uintptr_t>(module));
                FDGE_WIN_CHAR_TYPE buffer[MAX_PATH];
                if (GetModuleFileName(module, buffer, sizeof(buffer))) {
                    std::filesystem::path modulePath(buffer);
                    moduleName = modulePath.filename().string();
                } else {
                    Logger::Warn("Unable to lookup module name for address {:X}", reinterpret_cast<uintptr_t>(address));
                }
            } else {
                Logger::Warn("Unable to lookup owning module of address {:X}.", reinterpret_cast<uintptr_t>(address));
            }

            bool isSkyrim = moduleName == "SkyrimSE.exe" || moduleName == "SkyrimVR.exe";
            if (wasNativeFunction && isSkyrim) {
                break;
            }

            auto source = frame.source_file();
            if (source.empty()) {
                Logger::Warn("Attempted to get native stack trace, but no symbols file was found for stack "
                             "trace information.");
                break;
            }
            wasNativeFunction = source.ends_with(R"(\include\RE\N\NativeFunction.h)");
            auto* frameObject = new StackFrame(moduleName, source, static_cast<int32_t>(frame.source_line()), offset);
            auto papyrusObject = frameObject->Bind();
            if (papyrusObject) {
                _stackTrace.emplace_back(papyrusObject);
            } else {
                Logger::Warn("Failure to bind StackFrame script object while generating Error stack trace.");
            }
        }
    }

    for (auto* frame = stack; frame; frame = frame->previousFrame) {
        auto* frameObject = new StackFrame(frame);
        auto papyrusObject = frameObject->Bind();
        if (papyrusObject) {
            _stackTrace.emplace_back(papyrusObject);
        } else {
            Logger::Warn("Failure to bind StackFrame script object while generating Error stack trace.");
        }
    }
}

namespace {
    RegisterScriptType(Error);

    PapyrusClass(Error) {
        PapyrusStaticFunction(Create, std::string_view messageTemplate, std::vector<std::string_view> templateArguments,
                              Error* cause, bool includeStackTrace) -> Error* {
            std::vector<fmt::basic_format_arg<fmt::format_context>> args;
            for (const auto& arg : templateArguments) {
                args.push_back(fmt::detail::make_arg<fmt::format_context>(arg));
            }

            std::string reason;

            try {
                reason = fmt::vformat(messageTemplate, fmt::format_args(args.data(), static_cast<int>(args.size())));
            } catch (const fmt::format_error&) {
                return nullptr;
            }

            if (includeStackTrace) {
                auto* stack = CurrentCallStack.get();
                auto* top = stack->top->previousFrame;
                return new Error(reason, cause, top);
            } else {
                return new Error(reason, cause);
            }
        };

        PapyrusFunction(__GetReason, Error* self) {
            if (!self) {
                return ""sv;
            }
            return self->GetReason();
        };

        PapyrusFunction(__GetCause, Error* self) -> Error* {
            if (!self) {
                return nullptr;
            }
            return self->GetCause();
        };

        PapyrusFunction(__GetStackTrace, Error* self) {
            if (!self) {
                return std::vector<FDGE::Binding::Papyrus::StackFrame*>();
            }
            return self->GetStackTrace();
        };
    };
}
