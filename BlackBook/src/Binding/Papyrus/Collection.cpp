#include <FDGE/Binding/Papyrus/Collection.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

namespace {
    RegisterScriptType(Collection);
}

PapyrusClass(Collection) {
    PapyrusFunction(__GetSize, Collection* self) {
        if (!self) {
            return 0;
        }
        return self->GetSize();
    };

    PapyrusFunction(Clear, Collection* self) {
        if (!self) {
            return false;
        }
        return self->Clear();
    };

    PapyrusFunction(GetValueIterator, Collection* self) -> ValueIterator* {
        if (!self) {
            return nullptr;
        }
        return self->GetValueIterator();
    };
};
