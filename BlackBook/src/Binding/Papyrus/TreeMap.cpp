#include <FDGE/Binding/Papyrus/TreeMap.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

Any* TreeMap::Get(Any* key) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.find(key);
    if (result == _value.end()) {
        return nullptr;
    }
    return result->second.GetObject();
}

Any* TreeMap::Put(Any* key, Any* value) {
    std::unique_lock lock(_lock);
    ++_version;
    auto result = _value.try_emplace(key, value);
    if (result.second) {
        return nullptr;
    }
    auto* replaced = result.first->second.GetObject();
    result.first->second = value;
    return replaced;
}

bool TreeMap::TryPut(Any* key, Any* value) {
    std::unique_lock lock(_lock);
    auto result = _value.try_emplace(key, value);
    if (result.second) {
        ++_version;
    }
    return result.second;
}

bool TreeMap::Contains(Any* key) noexcept {
    std::unique_lock lock(_lock);
    return _value.find(key) != _value.end();
}

Any* TreeMap::Delete(Any* key) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.find(key);
    if (result == _value.end()) {
        return nullptr;
    }
    ++_version;
    auto* deleted = result->second.GetObject();
    _value.erase(result);
    return deleted;
}

bool TreeMap::Clear() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return false;
    }
    ++_version;
    _value.clear();
    return true;
}

int32_t TreeMap::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

ValueIterator* TreeMap::GetValueIterator() noexcept {
    return GetKeyValueIterator();
}

KeyValueIterator* TreeMap::GetKeyValueIterator() noexcept {
    return new TreeMapIterator(this);
}

TreeMapIterator::TreeMapIterator(TreeMap* parent, bool reverse) : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    if (parent->GetSize()) {
        _iter = reverse ? --parent->_value.end() : parent->_value.begin();
        if (IsValid()) {
            _key = _iter->first;
            _value = _iter->second;
        }
    } else {
        _iter = parent->_value.end();
        if (reverse) {
            _beforeBegin = true;
        }
    }
}

TreeMapIterator::TreeMapIterator(TreeMap* parent, bool reverse, Any* key) : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    _iter = parent->_value.find(key);
    if (IsValid()) {
        _key = _iter->first;
        _value = _iter->second;
    } else if (reverse) {
        _beforeBegin = true;
    }
}

void TreeMapIterator::OnGameLoadComplete() {
    auto* parent = _parent.GetObject();
    auto* key = _key.GetObject();
    if (!parent || !key) {
        return;
    }
    std::unique_lock lock(parent->_lock);
    _iter = parent->_value.find(key);
}

bool TreeMapIterator::IsValid() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    return _version == parent->_version && _iter != parent->_value.end() && !_beforeBegin;
}

bool TreeMapIterator::HasMore() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    return _iter != parent->_value.end() && !_beforeBegin;
}

bool TreeMapIterator::SetValue(Any* value) noexcept {
    TreeMap* parent = _parent;
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (!IsValid()) {
        return false;
    }
    _version = ++parent->_version;
    parent->_value[_key] = value;
    return true;
}

bool TreeMapIterator::Next() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (parent->_version != _version) {
        return false;
    }
    if (_reverse) {
        if (_iter == parent->_value.begin()) {
            _beforeBegin = true;
            _key = {};
            _value = {};
            return false;
        }
        --_iter;
    } else if (_iter != parent->_value.end()) {
        ++_iter;
    }
    if (!IsValid()) {
        _key = {};
        _value = {};
        return false;
    }
    _key = _iter->first;
    _value = _iter->second;
    return true;
}

Pair* TreeMapIterator::GetEntry() noexcept {
    if (!_key && !_value) {
        return nullptr;
    }
    return new Pair(_key, _value);
}

namespace {
    RegisterScriptType(TreeMap)
    RegisterScriptType(TreeMapIterator)

    PapyrusClass(TreeMap) {
        PapyrusStaticFunction(Create) {
            return new TreeMap();
        };

        PapyrusFunction(GetReverseKeyValueIterator, TreeMap* self) -> KeyValueIterator* {
            if (!self) {
                return nullptr;
            }
            return new TreeMapIterator(self, true);
        };

        PapyrusFunction(GetKeyValueIteratorFrom, TreeMap* self, Any* key) -> KeyValueIterator* {
            if (!self) {
                return nullptr;
            }
            return new TreeMapIterator(self, false, key);
        };

        PapyrusFunction(GetReverseKeyValueIteratorFrom, TreeMap* self, Any* key) -> KeyValueIterator* {
            if (!self) {
                return nullptr;
            }
            return new TreeMapIterator(self, true, key);
        };
    }
}
