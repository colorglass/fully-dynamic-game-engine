#include <FDGE/Binding/Papyrus/Assert.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Binding/Papyrus/ScriptObject.h>
#include <FDGE/Util/Maps.h>

using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Binding::Papyrus::Assert;
using namespace FDGE::Util;
using namespace gluino;
using namespace RE;
using namespace RE::BSScript;

#undef GetObject

namespace {
    parallel_flat_hash_map<VMHandle, std::vector<std::string>> AssertFailures;

    [[nodiscard]] inline std::string GetFormID(TESForm* form) noexcept {
        return form ? fmt::format("{:X}", form->GetFormID()) : "None";
    }

    [[nodiscard]] inline std::string GetActiveEffectName(ActiveEffect* effect) {
        return effect ? fmt::format("{:X}:{}", effect->GetTargetActor()->GetFormID(), effect->usUniqueID) : "None";
    }

    [[nodiscard]] inline std::string GetAliasName(BGSBaseAlias* alias) {
        return alias ? fmt::format("{:X}:{}", alias->owningQuest->GetFormID(), alias->aliasID) : "None";
    }

    PapyrusClass(Assert) {
        PapyrusStaticFunction(IsTrue, bool value, std::string_view message) {
            return TrackAssert(CurrentCallStack, value, message.empty() ? "Expected value to be true." : message);
        };

        PapyrusStaticFunction(IsFalse, bool value, std::string_view message) {
            return TrackAssert(CurrentCallStack, !value, message.empty() ? "Expected value to be false." : message);
        };

        PapyrusStaticFunction(AreIntsEqual, int32_t expected, int32_t actual, std::string_view message) {
            return TrackAssert(CurrentCallStack, expected == actual,
                               message.empty() ? fmt::format("Expected {}, but actual value was {}.", expected, actual)
                                               : message);
        };

        PapyrusStaticFunction(AreFloatsEqual, float_t expected, float_t actual, std::string_view message) {
            return TrackAssert(CurrentCallStack, expected == actual,
                               message.empty() ? fmt::format("Expected {}, but actual value was {}.", expected, actual)
                                               : message);
        };

        PapyrusStaticFunction(AreStringsEqual, std::string_view expected, std::string_view actual,
                              std::string_view message) {
            return TrackAssert(CurrentCallStack, str_equal_to<false>{}(expected, actual),
                               message.empty() ? fmt::format("Expected '{}', but actual value was '{}'.",
                                                             expected, actual)
                                               : message);
        };

        PapyrusStaticFunction(AreObjectsEqual, ScriptObject* expected, ScriptObject* actual, std::string_view message) {
            return TrackAssert(CurrentCallStack, (expected == nullptr && actual == nullptr) ||
                                                     (expected != nullptr && expected->Equals(actual)),
                               message.empty() ? "Objects were not equal as expected." : message);
        };

        PapyrusStaticFunction(AreFormsEqual, TESForm* expected, TESForm* actual, std::string_view message) {
            return TrackAssert(CurrentCallStack, expected == actual,
                               message.empty() ? fmt::format("Expected form {}, but actual value was {}.",
                                                             GetFormID(expected), GetFormID(actual))
                                               : message);
        };

        PapyrusStaticFunction(AreActiveMagicEffectsEqual, ActiveEffect* expected, ActiveEffect* actual,
                              std::string_view message) {
            return TrackAssert(
                CurrentCallStack, expected == actual,
                message.empty() ? fmt::format("Expected active effect {}, but actual value was {}.",
                                              GetActiveEffectName(expected), GetActiveEffectName(actual)) : message);
        };

        PapyrusStaticFunction(AreAliasesEqual, BGSBaseAlias* expected, BGSBaseAlias* actual, std::string_view message) {
            return TrackAssert(CurrentCallStack, expected == actual,
                               message.empty() ? fmt::format("Expected alias {}, but actual value was {}.",
                                                             GetAliasName(expected), GetAliasName(actual)) : message);
        };

        PapyrusStaticFunction(IsObjectNone, ScriptObject* object, std::string_view message) {
            return TrackAssert(CurrentCallStack, object == nullptr,
                               message.empty() ? "Expected object to be None." : message);
        };

        PapyrusStaticFunction(IsFormNone, TESForm* object, std::string_view message) {
            return TrackAssert(CurrentCallStack, object == nullptr,
                               message.empty() ? "Expected form to be None." : message);
        };

        PapyrusStaticFunction(IsActiveMagicEffectNone, ActiveEffect* object, std::string_view message) {
            return TrackAssert(CurrentCallStack, object == nullptr,
                               message.empty() ? "Expected active magic effect to be None." : message);
        };

        PapyrusStaticFunction(IsAliasNone, BGSBaseAlias* object, std::string_view message) {
            return TrackAssert(CurrentCallStack, object == nullptr,
                               message.empty() ? "Expected alias to be None." : message);
        };

        PapyrusStaticFunction(IsObjectNotNone, ScriptObject* object, std::string_view message) {
            return TrackAssert(CurrentCallStack, object != nullptr,
                               message.empty() ? "Expected object to not be None." : message);
        };

        PapyrusStaticFunction(IsFormNotNone, TESForm* object, std::string_view message) {
            return TrackAssert(CurrentCallStack, object != nullptr,
                               message.empty() ? "Expected form to not be None." : message);
        };

        PapyrusStaticFunction(IsActiveMagicEffectNotNone, ActiveEffect* object, std::string_view message) {
            return TrackAssert(CurrentCallStack, object != nullptr,
                               message.empty() ? "Expected active magic effect to not be None." : message);
        };

        PapyrusStaticFunction(IsAliasNotNone, BGSBaseAlias* object, std::string_view message) {
            return TrackAssert(CurrentCallStack, object != nullptr,
                               message.empty() ? "Expected alias to not be None." : message);
        };
    }
}

std::vector<std::string> FDGE::Binding::Papyrus::Assert::FindAssertFailures(RE::VMHandle handle) {
    std::vector<std::string> results;
    AssertFailures.if_contains(handle, [&](auto& value) {
        results = std::move(value.second);
    });
    return std::move(results);
}

bool FDGE::Binding::Papyrus::Assert::TrackAssert(BSTSmartPointer<Stack>&& stack, bool success,
                                                 std::string_view message) {
    if (success) {
        return success;
    }

    for (auto callingType = stack->top->previousFrame->owningObjectType.get();
         callingType; callingType = callingType->GetParent()) {
        if (str_equal_to<false>{}(callingType->GetName(), "TestSuite")) {
            auto self = stack->top->previousFrame->self.GetObject();
            auto failures = AssertFailures.try_emplace(self->GetHandle());

            uint32_t lineNumber;
            stack->top->previousFrame->owningFunction->TranslateIPToLineNumber(
                stack->top->previousFrame->instructionPointer, lineNumber);

            failures.first->second.emplace_back(fmt::format("Line {}: {}", lineNumber, message.data()));
        }
    }

    return success;
}
