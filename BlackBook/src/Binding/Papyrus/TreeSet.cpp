#include <FDGE/Binding/Papyrus/TreeSet.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

Any* TreeSet::GetValue(Any* value) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.find(value);
    if (result == _value.end()) {
        return nullptr;
    }
    return const_cast<Any*>(result->GetObject());
}

Any* TreeSet::PutValue(Any* value) noexcept {
    std::unique_lock lock(_lock);
    ++_version;
    auto result = _value.emplace(value);
    if (result.second) {
        return nullptr;
    }
    auto* replaced = const_cast<Any*>(result.first->GetObject());
    const_cast<ScriptObjectHandle<Any>&>(*result.first) = value;
    return replaced;
}

bool TreeSet::TryPutValue(Any* value) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.emplace(value);
    if (result.second) {
        ++_version;
    }
    return result.second;
}

bool TreeSet::Contains(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    return _value.find(value) != _value.end();
}

Any* TreeSet::Delete(Any* key) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.find(key);
    if (result == _value.end()) {
        return nullptr;
    }
    ++_version;
    auto* deleted = const_cast<Any*>(result->GetObject());
    _value.erase(result);
    return deleted;
}

bool TreeSet::Clear() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return false;
    }
    ++_version;
    _value.clear();
    return true;
}

int32_t TreeSet::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

ValueIterator* TreeSet::GetValueIterator() noexcept {
    return new TreeSetIterator(this, false);
}

ValueIterator* TreeSet::GetReverseValueIterator() noexcept {
    return new TreeSetIterator(this, true);
}

ValueIterator* TreeSet::GetValueIteratorFrom(Any* value) noexcept {
    return new TreeSetIterator(this, false, value);
}

ValueIterator* TreeSet::GetReverseValueIteratorFrom(Any* value) noexcept {
    return new TreeSetIterator(this, true, value);
}

TreeSetIterator::TreeSetIterator(TreeSet* parent, bool reverse) noexcept
        : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    if (parent->GetSize()) {
        _iter = reverse ? --parent->_value.end() : parent->_value.begin();
        if (IsValid()) {
            _value = *_iter;
        }
    } else {
        _iter = parent->_value.end();
        if (reverse) {
            _beforeBegin = true;
        }
    }
}

TreeSetIterator::TreeSetIterator(TreeSet* parent, bool reverse, Any* key) noexcept
        : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    _iter = parent->_value.find(key);
    if (IsValid()) {
        _value = *_iter;
    } else if (reverse) {
        _beforeBegin = true;
    }
}

void TreeSetIterator::OnGameLoadComplete() {
    auto* parent = _parent.GetObject();
    auto* value = _value.GetObject();
    if (!parent || !value) {
        return;
    }
    std::unique_lock lock(parent->_lock);
    _iter = parent->_value.find(value);
}

bool TreeSetIterator::IsValid() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    return _version == parent->_version && _iter != parent->_value.end() && !_beforeBegin;
}

bool TreeSetIterator::HasMore() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    return _iter != parent->_value.end() && !_beforeBegin;
}

Any* TreeSetIterator::GetValue() noexcept {
    return _value.GetObject();
}

bool TreeSetIterator::SetValue(Any* value) noexcept {
    TreeSet* parent = _parent;
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (!IsValid()) {
        return false;
    }
    parent->_value.erase(_iter);
    auto result = parent->TryPutValue(value);
    _version = parent->_version;
    return result;
}

bool TreeSetIterator::Next() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (parent->_version != _version) {
        return false;
    }
    if (_reverse) {
        if (_iter == parent->_value.begin()) {
            _beforeBegin = true;
            _value = {};
            return false;
        }
        --_iter;
    } else if (_iter != parent->_value.end()) {
        ++_iter;
    }
    if (!IsValid()) {
        _value = {};
        return false;
    }
    _value = *_iter;
    return true;
}

namespace {
    RegisterScriptType(TreeSet)
    RegisterScriptType(TreeSetIterator)

    PapyrusClass(TreeSet) {
        PapyrusStaticFunction(Create) {
            return new TreeSet();
        };

        PapyrusFunction(GetReverseValueIterator, TreeSet* self) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseValueIterator();
        };

        PapyrusFunction(GetReverseValueIteratorFrom, TreeSet* self, Any* value) -> ValueIterator* {
            if (!self) {
                return nullptr;
            }
            return self->GetReverseValueIteratorFrom(value);
        };
    }
}
