#include <FDGE/Dymod/RuleModule.h>

#include <FDGE/Dymod/DynamicEvent.h>
#include <FDGE/Dymod/Predicate.h>

using namespace FDGE;
using namespace FDGE::Dymod;

void RuleModule::operator()(const DynamicEvent& event) const noexcept {
    gluino::polymorphic_any obj(&event);
    for (auto& trigger : GetTriggers()) {
        if (trigger->Test(obj)) {
            Apply(event);
        }
    }
}
