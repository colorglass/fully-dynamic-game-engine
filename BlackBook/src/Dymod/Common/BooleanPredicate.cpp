#include <FDGE/Dymod/Common/BooleanPredicate.h>

using namespace FDGE::Dymod;
using namespace FDGE::Dymod::Common;

PredicateResult BooleanPredicate::TestImpl(const gluino::polymorphic_any& target) const noexcept {
    auto maybe = GetValue(target);
    if (!maybe.has_value()) {
        return PredicateResult::NotApplicable;
    }
    if (_negated) {
        return maybe.value() ? PredicateResult::False : PredicateResult::True;
    } else {
        return maybe.value() ? PredicateResult::True : PredicateResult::False;
    }
}
