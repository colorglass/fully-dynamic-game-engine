#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Skyrim;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"lua"}, [](Execution& execution) {
                        // TODO
                    }));
        });
    }
}
