#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"FindReferences", "findref"}, [](Execution& execution) {
                        auto actorTarget = execution.OptionalTarget<TESObjectREFR*>();
                        auto baseTarget = execution.OptionalTarget<TESForm*>();
                        auto activeOnly = execution.Flag("active", "a");

                        if (actorTarget.has_value()) {
                            baseTarget = actorTarget.value()->GetBaseObject();
                        }
                        if (baseTarget.has_value()) {
                            auto* target = baseTarget.value();
                            if (activeOnly) {
                                for (auto actorHandle : RE::ProcessLists::GetSingleton()->highActorHandles) {
                                    auto actor = actorHandle.get();
                                    if (!actor) {
                                        continue;
                                    }
                                    auto* staticBase = actor->GetActorBase();
                                    TESActorBase* dynamicBase = nullptr;
                                    auto* leveledData = actor->extraList.GetByType<ExtraLeveledCreature>();
                                    if (leveledData) {
                                        dynamicBase = leveledData->originalBase;
                                    }
                                    if (staticBase == target || dynamicBase == target) {
                                        Print("{:X}", actor->GetFormID());
                                    }
                                }
                            } else {
                                auto allForms = TESForm::GetAllForms();
                                BSReadLockGuard lock(allForms.second.get());
                                for (auto& entry : *allForms.first) {
                                    auto* ref = entry.second->As<TESObjectREFR>();
                                    if (ref) {
                                        if  (ref->GetBaseObject() == target) {
                                            Print("{:X}", ref->GetFormID());
                                        }
                                    }
                                }
                            }
                        } else {
                            Print("FindReferences requires a target that is an actor reference or base object.");
                        }
                    }));
        });
    }
}
