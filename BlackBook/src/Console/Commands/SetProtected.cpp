#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Skyrim;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"setprotected"}, [](Execution& execution) {
                        auto actorTarget = execution.OptionalTarget<Actor*>();
                        auto baseTarget = execution.OptionalTarget<RE::TESNPC*>();
                        auto value = execution.NextPositional<bool>("value");

                        if (actorTarget.has_value()) {
                            auto* actor = actorTarget.value();
                            baseTarget = actor->GetActorBase();
                            if (value) {
                                actor->boolFlags.set(Actor::BOOL_FLAGS::kProtected);
                            } else {
                                actor->boolFlags.reset(Actor::BOOL_FLAGS::kProtected);
                            }
                        }
                        if (baseTarget.has_value()) {
                            auto* base = baseTarget.value()->As<TESActorBaseData>();
                            if (value) {
                                base->actorData.actorBaseFlags.set(ACTOR_BASE_DATA::Flag::kProtected);
                            } else {
                                base->actorData.actorBaseFlags.reset(ACTOR_BASE_DATA::Flag::kProtected);
                            }
                        } else {
                            Print("SetProtected requires a target that is an actor reference or base NPC.");
                        }
                    }));
        });
    }
}
