#include <FDGE/Console/Parser.h>

#define PEGLIB_NO_UNICODE_CHARS
#undef assert

#include <FDGE/Logger.h>
#include <peglib.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace gluino;

namespace {
    struct Argument {
        bool IsArgument{false};
        bool IsShort{false};
        std::string Key;
        std::optional<std::string> Value;
    };

    struct Result {
        std::string ExplicitTarget;
        std::string Command;
        std::vector<Argument> Arguments{};
    };
}

namespace FDGE::Console::detail {
    class CommandGrammar {
    public:
        explicit CommandGrammar(Parser& parser) : _parser(parser) {
        }

        static void Initialize() {
            // TODO: Handle subcommands.
            // TODO: Support parsing subcommands for target.
            // TODO: Handle quoted string escapes.
            std::string_view peg = R"(
                Command <- (_? Target Dot _? CommandName Arguments? _?) / (_? At _? CommandName Arguments? _?) / (_? CommandName Arguments? _?)
                Target <- !At (SubCommand / TargetString)
                CommandName <- [a-zA-Z] ('-' / [a-zA-Z0-9_])* ('::' [a-zA-Z] ('-' / [a-zA-Z0-9_])*)*
                ShortParameter <- [a-zA-Z]
                LongParameter <- Key (Equals Term)?
                Parameters <- LongPrefix LongParameter / ShortPrefix ShortParameter+
                Arguments <- (_ Parameters)* (_ Argument)*
                Argument <- Term
                Term <- SubCommand / QuotedString / String
                QuotedStringContents <- ('\\' '"' / !QuotationMark .)*
                QuotedString <- QuotationMark QuotedStringContents QuotationMark
                String <- (![ $"] .)+
                TargetString <- (!(Dot _? CommandName Arguments? _? !.) .)+
                Key <- (![ =$] .)+
                SubCommand <- '${' _? Command _? '}'
                ~At <- '@'
                ~Dot <- '.'
                ~ShortPrefix <- '-'
                ~LongPrefix <- '--'
                ~Equals <- '='
                ~QuotationMark <- '"'
                ~_ <- [ \t]+
            )";

#ifndef NDEBUG
            // This is duplicate parsing to what is done internally when we call load_grammar, but the only way to sink
            // any errors in parsing to the log. This should only be relevant during development, so it is not enabled
            // for release builds.
            std::string start;
            peg::ParserGenerator::parse(peg.data(), peg.size(), start, [](std::size_t line, std::size_t column,
                    const std::string& msg) {
                Logger::Error("PEG grammar error at line {}, column {}: {}", line, column, msg);
            });
#endif
            _grammar.load_grammar(peg.data(), peg.size());

            _grammar["Command"] = [](const peg::SemanticValues& values) {
                Result result;
                std::size_t nextIndex = 0;
                switch (values.choice()) {
                    case 0:
                        result.ExplicitTarget = values[nextIndex++].template get<std::string>();
                        result.Command = values[nextIndex++].template get<std::string>();
                        break;
                    case 1:
                        result.ExplicitTarget = ".";
                    case 2:
                        result.Command = values[nextIndex++].template get<std::string>();
                        break;
                    default:
                        throw std::invalid_argument("Invalid parser choice found. This is an implementation bug.");
                }
                if (nextIndex < values.size()) {
                    auto& args = values[nextIndex].template get<std::vector<Argument>>();
                    for (auto entry : args) {
                        result.Arguments.emplace_back(std::move(entry));
                    }
                }
                return result;
            };
            _grammar["Target"] = [](const peg::SemanticValues& values) {
                auto str = values.str();
                if (str.empty()) {
                    str = ".";
                }
                return str;
            };
            auto strReturn = [](const peg::SemanticValues& values) {
                return values.str();
            };
            _grammar["CommandName"] = strReturn;
            _grammar["Key"] = strReturn;
            _grammar["String"] = strReturn;
            _grammar["TargetString"] = strReturn;
            _grammar["QuotedStringContents"] = strReturn;
            _grammar["Term"] = [](const peg::SemanticValues& values) {
                return values[0].template get<std::string>();
            };
            _grammar["Argument"] = [](const peg::SemanticValues& values) {
                return std::vector<Argument>{Argument{true, false, values[0].template get<std::string>(), {}}};
            };
            _grammar["QuotedString"] = [](const peg::SemanticValues& values) {
                return values[0].template get<std::string>();
            };
            _grammar["Arguments"] = [](const peg::SemanticValues& values) {
                std::vector<Argument> arguments;
                for (const auto& args : values) {
                    for (const auto& arg : args.template get<std::vector<Argument>>()) {
                        arguments.emplace_back(arg);
                    }
                }
                return arguments;
            };
            _grammar["Parameters"] = [](const peg::SemanticValues& values) {
                std::vector<Argument> result;
                for (auto& arg : values) {
                    result.emplace_back(arg.template get<Argument>());
                }
                return result;
            };
            _grammar["ShortParameter"] = [](const peg::SemanticValues& values) {
                return Argument{false, true, values.str()};
            };
            _grammar["LongParameter"] = [](const peg::SemanticValues& values) {
                if (values.size() == 1) {
                    return Argument{false, false, values[0].template get<std::string>(), {}};
                } else {
                    return Argument{false, false, values[0].template get<std::string>(),
                            values[1].template get<std::string>()};
                }
            };
        }

        bool Parse(std::string_view input) {
            Result result;
            auto success = _grammar.parse(input.data(), result, "Command");
            _parser._valid = success;
            if (!success) {
                return false;
            }
            _parser._valid = true;
            if (result.ExplicitTarget == ".") {
                _parser._explicitlyNonTargeting = true;
            } else {
                _parser._explicitTarget = std::move(result.ExplicitTarget);
            }
            auto namespaceEnd = result.Command.find_last_of("::");
            if (namespaceEnd == std::string::npos) {
                _parser._command = std::move(result.Command);
            } else {
                _parser._namespace = result.Command.substr(0, namespaceEnd);
                _parser._command = result.Command.substr(namespaceEnd + 2);
            }
            bool noRepeats = true;
            for (auto& entry : result.Arguments) {
                if (entry.IsArgument) {
                    _parser._positionalArguments.emplace_back(std::move(entry.Key));
                }
                else if (entry.Value.has_value()) {
                    noRepeats &= _parser._parameters.try_emplace(std::move(entry.Key),
                                                                 std::move(entry.Value.value())).second;
                } else {
                    if (entry.IsShort) {
                        noRepeats &= _parser._shortFlags.emplace(std::move(entry.Key)).second;
                    } else {
                        noRepeats &= _parser._flags.emplace(std::move(entry.Key)).second;
                    }
                }
            }
            if (!noRepeats) {
                throw std::invalid_argument("Argument flag can only be specified once.");
            }
            return true;
        }

    private:
        static inline peg::parser _grammar;
        Parser& _parser;
    };
}

namespace {
    gluino_autorun {
        FDGE::Console::detail::CommandGrammar::Initialize();
    }
}

Parser::Parser(std::string_view input) {
    FDGE::Console::detail::CommandGrammar grammar(*this);
    grammar.Parse(input);
}

Parser::~Parser() {
}

std::string_view Parser::GetExplicitTarget() const noexcept {
    return _explicitTarget;
}

std::string_view Parser::GetNamespace() const noexcept {
    return _namespace;
}

std::string_view Parser::GetCommand() const noexcept {
    return _command;
}

std::string_view Parser::GetParameter(std::string_view fullName) const {
    auto result = _parameters.find(fullName);
    if (result != _parameters.end()) {
        return result->second;
    }
    if (_flags.contains(fullName)) {
        throw std::invalid_argument(fmt::format("Parameter '--{}' requires a value.", fullName));
    }
    return "";
}

bool Parser::HasFlag(std::string_view fullName, std::string_view shortName) const {
    auto result = _flags.find(fullName);
    if (result != _flags.end()) {
        return true;
    }
    result = _shortFlags.find(shortName);
    if (result != _shortFlags.end()) {
        return true;
    }
    if (_parameters.contains(fullName) || _parameters.contains(shortName)) {
        if (shortName.empty()) {
            throw std::invalid_argument(
                    fmt::format("Parameter '--{}' does not take a value, it should be used as a flag.", fullName));
        } else {
            throw std::invalid_argument(
                    fmt::format("Parameter '--{}' (or '-{}') does not take a value, it should be used as a flag.",
                                fullName, shortName));
        }
    }
    return false;
}

bool Parser::NextPositionalArgument(std::string_view& out) const noexcept {
    if (_position < _positionalArguments.size()) {
        auto& result = _positionalArguments[_position];
        ++_position;
        out = result;
        return true;
    }
    return false;
}
